import React, {useState} from 'react';
import './App.css';
import { UserOutlined, MenuUnfoldOutlined, DeliveredProcedureOutlined, FileProtectOutlined, MenuFoldOutlined, DatabaseOutlined, ExceptionOutlined, FileDoneOutlined } from '@ant-design/icons';
import { Layout, Menu, Button } from 'antd';
import InteractionRequest from "./content/InteractionRequest/InteractionRequest";
import InformationForTransferRequest from "./content/InformationForTransferRequest/InformationForTransferRequest";
import InformationForTransferAndAccrualReceiving from "./content/InformationForTransferAndAccrualReceiving/InformationForTransferAndAccrualReceiving";
import InformationForTransferWithAcceptanceReceiving from "./content/InformationForTransferWithAcceptanceReceiving/InformationForTransferWithAcceptanceReceiving";
import InformingAboutAcceptanceOfOrderForExecution from "./content/InformingAboutAcceptanceOfOrderForExecution/InformingAboutAcceptanceOfOrderForExecution";
import ReceivingRequestForTransferInformation from "./content/ReceivingRequestForTransferInformation/ReceivingRequestForTransferInformation";
import {findObjectByKey} from "./utils";
const { Header, Content, Sider } = Layout;

function App() {
  const [collapsed, setCollapsed] = useState(false);
  const [currentTabFromLocalstorage] = useState(localStorage.getItem('currentTab'));
  const [currentContent, setCurrentContent] = useState<string>(currentTabFromLocalstorage !== null ? currentTabFromLocalstorage : '1');

  const items = [
      {
          key: '1',
          icon: <UserOutlined />,
          label: 'Заявка на взаимодействие',
          content: <InteractionRequest />,
      },
      {
          key: '2',
          icon: <DeliveredProcedureOutlined />,
          label: 'Запрос информации для перевода',
          content: <InformationForTransferRequest />
      },
      {
          key: '3',
          icon: <DatabaseOutlined />,
          label: 'Получение информации для перевода с акцептом',
          content: <InformationForTransferWithAcceptanceReceiving />,
      },
      {
          key: '4',
          icon: <FileDoneOutlined />,
          label: 'Получение информации для перевода, о начислении',
          content: <InformationForTransferAndAccrualReceiving />,
      },
      {
          key: '5',
          icon: <FileProtectOutlined />,
          label: 'Информирование о приеме к исполнению распоряжения',
          content: <InformingAboutAcceptanceOfOrderForExecution />
      },
      {
          key: '6',
          icon: <ExceptionOutlined />,
          label: 'Получение запроса информации о переводе',
          content: <ReceivingRequestForTransferInformation />
      },
  ];

  return (
      <Layout style={{height: '100%'}}>
          <Sider width={400} trigger={null} collapsible collapsed={collapsed}>
              <div className="demo-logo-vertical" />
              <Menu
                  theme="dark"
                  mode="inline"
                  defaultSelectedKeys={[currentContent]}
                  onClick={(event) => {
                      setCurrentContent(event.key);
                      localStorage.setItem('currentTab', event.key);
                  }}
                  items={items}
              />
          </Sider>
          <Layout>
              <Header style={{ padding: 0, background: "white" }}>
                  <Button
                      type="text"
                      icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                      onClick={() => setCollapsed(!collapsed)}
                      style={{
                          fontSize: '16px',
                          width: 64,
                          height: 64,
                      }}
                  />
              </Header>
              <Content
                  style={{
                      margin: '24px 16px',
                      padding: 24,
                      minHeight: 280,
                      background: "white",
                      borderRadius: 20,
                  }}
              >
                  {findObjectByKey(items,'key' , currentContent)}
              </Content>
          </Layout>
      </Layout>
  );
}

export default App;
