export function findObjectByKey(array: any[], keyToFind: string, value: string) {
    const foundObject = array.find(obj => obj[keyToFind] === value);

    if (foundObject) {
        return foundObject.content;
    } else {
        return (<div>Ошибка</div>);
    }
}

export function formatDateToCustomString(date: string | undefined | number, key: string) {
    if (date === undefined) {
        return;
    }

    const isHoursDisplayed = key === 'dateOfCreation' || key === 'dateOfDispatch' || key === 'dateOfReceiptOfResponse' || key === 'dateOfResponseFormation' || key === 'dateOfRequestFormation';
    const dateFromProps = new Date(date);

    const day = dateFromProps.getDate().toString().padStart(2, '0');
    const month = (dateFromProps.getMonth() + 1).toString().padStart(2, '0');
    const year = dateFromProps.getFullYear().toString().slice(2);
    const hours = dateFromProps.getHours().toString().padStart(2, '0');
    const minutes = dateFromProps.getMinutes().toString().padStart(2, '0');
    const seconds = dateFromProps.getSeconds().toString().padStart(2, '0');

    if (isHoursDisplayed) {
        return `${day}.${month}.${year} ${hours}:${minutes}:${seconds}`;
    }
    return `${day}.${month}.${year}`;
}

export const sourceOfRequestEnum = {
    global: 'Глобально',
    local: 'Локально'
}