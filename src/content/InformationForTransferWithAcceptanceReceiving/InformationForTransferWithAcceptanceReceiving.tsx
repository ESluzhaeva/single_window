import React, {useState} from 'react';
import {Button, Card, Col, Divider, Row, Statistic, Tag, Typography} from 'antd';
import {useAppSelector} from "../../store";
import {useDispatch} from "react-redux";
import {DeleteOutlined, DownOutlined, EditOutlined, PlusOutlined, UpOutlined} from "@ant-design/icons";
import {formatDateToCustomString, sourceOfRequestEnum} from "../../utils";
import InformationForTransferWithAcceptanceReceivingForm from "./InformationForTransferWithAcceptanceReceivingForm";
import {
    deleteInformationForTransferWithAcceptanceReceiving
} from "../../store/InformationForTransferWithAcceptanceReceiving";

const {Title} = Typography;

function InformationForTransferWithAcceptanceReceiving() {
    const arrayWithInformationForTransferWithAcceptanceReceiving = useAppSelector(
        (state) => state.arrayWithInformationForTransferWithAcceptanceReceiving.arrayWithInformationForTransferWithAcceptanceReceiving,
    );
    const [isCreation, setIsCreation] = useState(false);
    const [editingCard, setEditingCard] = useState<string | undefined>(undefined);
    const [isCardExpanded, setIsCardExpanded] = useState<string | undefined>(undefined);
    const dispatch = useDispatch();

    const baseFields = ['status', 'statusNumber', 'documentId', 'dateOfCreation', 'sourceOfRequest', 'dateOfResponseFormation'];

    const additionalFieldsLabels = {
        idOfRequest: 'Идентификатор запроса',
        dateOfRequestFormation: "Дата и время формирования запроса",
        routingCode: 'Код маршрутизации',
        idOfEnvelopeRequestInVIS: "Идентификатор запроса конверта в ВИС",
        idOfEnvelopeResponseInVIS: "Идентификатор ответа конверта в ВИС",
        acceptanceAmount: 'Сумма акцепта',
        payerOfAcceptanceAccountNumber: "Номер счета плательщика",
        bikOfPayersOfAcceptanceBank: "БИК банка плательщика",
        nameOfFoundationDocument: "Наименование документа-основания",
        numberOfFoundationDocument: "Номер документа-основания",
        dateOfFoundationDocument: "Дата документа-основания",
        nameOfPayersBankOfAcceptance: "Наименование банка плательщика",
        correspondentAccountOfPayersOfAcceptanceBank: "Корреспондентский счет банка плательщика",
        dateOfExpirationOfAuthority: "Дата окончания действия полномочий",
        electronicAccountNumber: 'Номер электронного счета',
        dateOfFormationOfElectronicAccount: "Дата формирования электронного счета",
        NDSRateAsPercentage: "Ставка НДС, %",
        amountOfNDS: "Сумма НДС",
        invoiceAmount: "Сумма счета",
        amountToBePaid: "Сумма к оплате",
        currencyCodeOfAmount: "Код валюты суммы",
        purposeOfPayment: "Назначение платежа",
        indicationOfNDSInclusionInAmount: "Признак включения НДС в сумму",
        validityPeriod: "Срок действия",
        nameOfRecipient: "Наименование получателя",
        innOfRecipient: "ИНН получателя",
        recipientsAccountNumber: "Номер счета получателя",
        nameofRecipientsBank: "Наименование банка получателя",
        bicOfRecipientsBank: "БИК банка получателя",
        kppOfRecipient: "КПП получателя",
        nameofBank: "Наименование банка получателя",
        correspondentAccountOfRecipientsBank: "Корреспондентский счет банка получателя",
        nameOfPayersBank: "Наименование банка плательщика",
        innOfPayersBank: "ИНН банка плательщика",
        kppOfPayersBank: "КПП банка плательщика",
        responseId: "Идентификатор ответа",
        codeOfProcessingResult: "Код результата обработки",
        errorCode: "Код ошибки",
        errorDescription: "Описание ошибки",
        idOfQualificationCertificate: "Уникальный номер квалифицированного сертификата",
        dateOfStartOfQualifiedCertificate: "Дата начала действия квалифицированного сертификата",
        dateOfEndOfQualifiedCertificate: "Дата окончания действия квалифицированного сертификата",
        fullNameInQualifiedCertificate: "ФИО",
        snilsInQualifiedCertificate: "СНИЛС",
        innInQualifiedCertificate: "ИНН",
        electronicSignature: "Электронная подпись",
    }

    return isCreation ? (
        <InformationForTransferWithAcceptanceReceivingForm
            onSaveButtonClick={() => setIsCreation(false)}
            onCancelButtonClick={() => setIsCreation(false)}
        />
    ) : ((
        <div>
            {!isCreation && !editingCard && (
                <Row justify='space-between'>
                    <Title style={{margin: '0 0 30px 0'}} level={3}>Получение информации для перевода с акцептом</Title>
                    <Button onClick={() => setIsCreation(true)} type="primary" icon={<PlusOutlined/>} size="large"/>
                </Row>
            )}

            {editingCard && (
                <InformationForTransferWithAcceptanceReceivingForm
                    key={editingCard}
                    record={arrayWithInformationForTransferWithAcceptanceReceiving.filter((value) => value.dateOfCreation === editingCard)[0]}
                    onSaveButtonClick={() => setEditingCard(undefined)}
                    onCancelButtonClick={() => setEditingCard(undefined)}
                />
            )}

            <Col span={32}>
                {!editingCard && arrayWithInformationForTransferWithAcceptanceReceiving.map((card) => (
                    <Card style={{marginBottom: '20px'}} key={card.dateOfCreation} title={
                        <Row justify='space-between'>
                            <div>Статус: <Tag color="lime">{card.status}</Tag></div>
                            <div>
                                <Button
                                    style={{marginRight: '10px'}}
                                    onClick={() => setEditingCard(card.dateOfCreation)}
                                    icon={<EditOutlined/>}
                                />
                                <Button
                                    onClick={() => dispatch(deleteInformationForTransferWithAcceptanceReceiving(card.dateOfCreation))}
                                    icon={<DeleteOutlined/>}
                                />
                            </div>
                        </Row>
                    }>
                        <Row style={{marginBottom: '30px'}} justify={"space-between"}>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Идентификатор документа"
                                       value={card.documentId}/>
                            <Statistic style={{marginRight: '10px'}} title="Дата создания"
                                       value={formatDateToCustomString(card.dateOfCreation, 'dateOfCreation')}/>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Код статуса"
                                       value={card.statusNumber}/>
                            {/*@ts-ignore*/}
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Источник запроса" value={sourceOfRequestEnum[card.sourceOfRequest]}/>
                            {card.dateOfResponseFormation && (
                                <Statistic
                                    style={{marginRight: '10px'}}
                                    title="Дата и время формирования ответа"
                                    value={formatDateToCustomString(card.dateOfResponseFormation, 'dateOfResponseFormation')}
                                />
                            )
                            }
                        </Row>

                        <Divider/>

                        <div style={{display: 'grid', gridTemplateColumns: 'repeat(4, 1fr)', gap: '16px'}}>
                            {isCardExpanded === card.dateOfCreation && Object.entries(card).filter(([key]) => !baseFields.includes(key))
                                .map(([key, value]) => (
                                    (value !== undefined && (
                                        (key === 'dateOfRequestFormation' || key === 'dateOfFoundationDocument' ||
                                            key === 'dateOfExpirationOfAuthority' || key === 'validityPeriod' ||
                                            key === 'dateOfFormationOfElectronicAccount' || key === 'dateOfStartOfQualifiedCertificate' ||
                                            key === 'dateOfEndOfQualifiedCertificate') ? (
                                            <div key={key}>
                                                {/*@ts-ignore*/}
                                                <Statistic groupSeparator={''} title={additionalFieldsLabels[key]} value={formatDateToCustomString(value, key)}
                                                />
                                            </div>
                                        ) : (
                                            <div key={key}>
                                                {/*@ts-ignore*/}
                                                <Statistic groupSeparator={''} title={additionalFieldsLabels[key]} value={value}/>
                                            </div>
                                        )
                                    )
                                )))
                            }
                        </div>

                        <Row style={isCardExpanded === card.dateOfCreation ? {marginTop: '20px'} : undefined}>
                            <Button
                                onClick={() => setIsCardExpanded((prev) => prev === undefined ? card.dateOfCreation : undefined)}
                                icon={isCardExpanded !== card.dateOfCreation ? <DownOutlined/> : <UpOutlined/>}
                                size="middle"
                            >
                                Доп. данные
                            </Button>
                        </Row>
                    </Card>
                ))}
            </Col>
        </div>
    ))
}

export default InformationForTransferWithAcceptanceReceiving;
