import React, {useState} from 'react';
import {Button, Form, Input, Select, Row, DatePicker, Col, Divider, Badge, Upload} from 'antd';
import {
    InformationForTransferAndAccrualReceivingType,
} from "../../types";
import dayjs from 'dayjs';
import {useAppDispatch} from "../../store";
import {UploadOutlined} from '@ant-design/icons';
import {
    addInformationForTransferAndAccrualReceiving,
    editInformationForTransferAndAccrualReceiving
} from "../../store/InformationForTransferAndAccrualReceiving";

const {TextArea} = Input;

type PropsType = {
    record?: InformationForTransferAndAccrualReceivingType,
    onSaveButtonClick: () => void;
    onCancelButtonClick: () => void;
}

function InformationForTransferAndAccrualReceivingForm({
                                                           record,
                                                           onSaveButtonClick,
                                                           onCancelButtonClick
                                                       }: PropsType) {
    const [form] = Form.useForm();
    const statusFields = ['status', 'statusNumber', 'dateOfCreation'];
    const dispatch = useAppDispatch();
    const [isFileUploaded, setIsFileUploaded] = useState<string | undefined>(record?.electronicSignature);

    return (
        <Form
            name="InformationForTransferAndAccrualReceiving"
            autoComplete="off"
            layout={'vertical'}
            form={form}
            initialValues={{
                documentId: record?.documentId,
                dateOfCreation: record?.dateOfCreation ? dayjs(record?.dateOfCreation) : undefined,
                sourceOfRequest: record?.sourceOfRequest,
                dateOfResponseFormation: record?.dateOfResponseFormation ? dayjs(record?.dateOfResponseFormation) : undefined,
                idOfEnvelopeRequestInVIS: record?.idOfEnvelopeRequestInVIS,
                idOfEnvelopeResponseInVIS: record?.idOfEnvelopeResponseInVIS,
                idOfRequest: record?.idOfRequest,
                dateOfRequestFormation: record?.dateOfRequestFormation ? dayjs(record.dateOfRequestFormation) : undefined,
                routingCode: record?.routingCode,
                reasonForReferral: record?.reasonForReferral,
                idOfAccrual: record?.idOfAccrual,
                nameOfRecipientOfAccrual: record?.nameOfRecipientOfAccrual,
                purposeOfAccrualPayment: record?.purposeOfAccrualPayment,
                transferOfAccrualAmount: record?.transferOfAccrualAmount,
                electronicAccountNumber: record?.electronicAccountNumber,
                electronicSignature: record?.electronicSignature,
                dateOfFormationOfElectronicAccount: record?.dateOfFormationOfElectronicAccount ? dayjs(record.dateOfFormationOfElectronicAccount) : undefined,
                indicationOfNDSInclusionInAmount: record?.indicationOfNDSInclusionInAmount,
                NDSRateAsPercentage: record?.NDSRateAsPercentage,
                amountOfNDS: record?.amountOfNDS,
                invoiceAmount: record?.invoiceAmount,
                amountToBePaid: record?.amountToBePaid,
                currencyCodeOfAmount: record?.currencyCodeOfAmount,
                purposeOfPayment: record?.purposeOfPayment,
                validityPeriod: record?.validityPeriod ? dayjs(record?.validityPeriod) : undefined,
                nameOfRecipient: record?.nameOfRecipient,
                innOfRecipient: record?.innOfRecipient,
                kppOfRecipient: record?.kppOfRecipient,
                recipientsAccountNumber: record?.recipientsAccountNumber,
                nameofRecipientsBank: record?.nameofRecipientsBank,
                bicOfRecipientsBank: record?.bicOfRecipientsBank,
                correspondentAccountOfRecipientsBank: record?.correspondentAccountOfRecipientsBank,
                nameOfPayersBank: record?.nameOfPayersBank,
                innOfPayersBank: record?.innOfPayersBank,
                kppOfPayersBank: record?.kppOfPayersBank,
                responseId: record?.responseId,
                codeOfProcessingResult: record?.codeOfProcessingResult,
                errorCode: record?.errorCode,
                errorDescription: record?.errorDescription,
                idOfQualificationCertificate: record?.idOfQualificationCertificate,
                dateOfStartOfQualifiedCertificate: record?.dateOfStartOfQualifiedCertificate ? dayjs(record?.dateOfStartOfQualifiedCertificate) : undefined,
                dateOfEndOfQualifiedCertificate: record?.dateOfEndOfQualifiedCertificate ? dayjs(record?.dateOfEndOfQualifiedCertificate) : undefined,
                fullNameInQualifiedCertificate: record?.fullNameInQualifiedCertificate,
                snilsInQualifiedCertificate: record?.snilsInQualifiedCertificate,
                innInQualifiedCertificate: record?.innInQualifiedCertificate
            }}
            onFinish={() => {
                if (form.isFieldsValidating()) {
                    return;
                }

                if (record?.dateOfCreation !== undefined) {
                    if (form.getFieldValue('dateOfCreation') !== undefined) {
                        dispatch(editInformationForTransferAndAccrualReceiving({
                            id: record.dateOfCreation,
                            key: "dateOfCreation",
                            value: form.getFieldValue('dateOfCreation').toISOString(),
                        }));
                    }
                    Object.entries(form.getFieldsValue()).filter(([key]) => !statusFields.includes(key))
                        .map(([value]) => {
                            const isDateValue =
                                value === 'dateOfRequestFormation' ||  value === 'validityPeriod' ||
                                value === 'dateOfFormationOfElectronicAccount' || value === 'dateOfStartOfQualifiedCertificate' ||
                                value === 'dateOfEndOfQualifiedCertificate' || value === 'dateOfResponseFormation';

                            if (form.isFieldTouched(value)) {
                                dispatch(editInformationForTransferAndAccrualReceiving({
                                    id: form.getFieldValue('dateOfCreation').toISOString(),
                                    key: value,
                                    value: isDateValue ? form.getFieldValue(value).toISOString() : (value === 'electronicSignature' ? form.getFieldValue(value).file.name : form.getFieldValue(value)),
                                }));
                            }
                        });
                } else {
                    dispatch(addInformationForTransferAndAccrualReceiving({
                        ...form.getFieldsValue(),
                        status: 'Новый',
                        statusNumber: 1,
                        dateOfCreation: form.getFieldValue('dateOfCreation').toISOString(),
                        electronicSignature: form.getFieldValue('electronicSignature').file.name,
                    }))
                }
                onSaveButtonClick();
            }}
        >
            <Row>
                <Col span={8}>
                    <Form.Item
                        name="documentId"
                        style={{marginRight: '50px'}}
                        label="Идентификатор документа"
                        rules={[{required: true, message: 'Введите идентификатор документа'}]}
                    >
                        <Input placeholder="Введите идентификатор документа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'Введите дату создания'}]}
                        style={{marginRight: '50px'}}
                        name="dateOfCreation" label="Дата и время создания"
                    >
                        <DatePicker showTime placeholder="Введите дату создания"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item rules={[{required: true, message: 'Выберите источник запроса'}]} name="sourceOfRequest" label="Источник запроса">
                        <Select placeholder='Выберите источник запроса'>
                            <Select.Option value="local">Локально</Select.Option>
                            <Select.Option value="global">Глобально</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        name="idOfEnvelopeRequestInVIS"
                        style={{marginRight: '50px'}}
                        label="Идентификатор запроса конверта в ВИС"
                    >
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="idOfEnvelopeResponseInVIS"
                               label="Идентификатор ответа конверта в ВИС">
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfResponseFormation" label="Дата и время формирования ответа">
                        <DatePicker showTime placeholder="Введите дату и время формирования ответа"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты запроса
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        rules={[{required: true, message: 'Введите идентификатор запроса'}]}
                        name="idOfRequest"
                        label="Идентификатор запроса"
                    >
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item rules={[{required: true, message: 'Введите дату и время формирования запроса'}]}
                               name="dateOfRequestFormation"
                               label="Дата и время формирования запроса"
                    >
                        <DatePicker showTime placeholder="Введите дату и время формирования запроса"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="routingCode"
                        label="Код маршрутизации"
                        rules={[{required: true, message: 'Введите код маршрутизации'}]}
                    >
                        <Input placeholder="Введите код маршрутизации"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="reasonForReferral"
                        label="Причина направления"
                        rules={[{required: true, message: 'Введите причину направления'}]}
                    >
                        <Input placeholder="Введите причину направления"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="electronicSignature" label="Электронная подпись">
                        <Upload
                            showUploadList={false}
                            onChange={(file) => {
                                setIsFileUploaded(file.file.name);
                            }}
                        >
                            <Button icon={<UploadOutlined />}>Загрузите файл</Button>
                        </Upload>
                        {isFileUploaded && (
                            <div style={{color: 'green'}}>
                                {isFileUploaded}
                            </div>
                        )}
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Информация о начислении
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item
                        name="idOfAccrual"
                        style={{marginRight: '50px'}}
                        label="Уникальный идентификатор начисления"
                    >
                        <Input placeholder="Введите идентификатор"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="nameOfRecipientOfAccrual"
                        label="Наименование получателя"
                    >
                        <Input placeholder="Введите наименование получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="transferOfAccrualAmount" label="Сумма перевода">
                        <Input placeholder="Введите сумму перевода"/>
                    </Form.Item>
                </Col>
            </Row>

            <Col span={16}>
                <Form.Item style={{marginRight: '50px'}} name="purposeOfAccrualPayment" label="Назначение платежа">
                    <TextArea
                        placeholder="Введите назначение платежа"
                        autoSize={{minRows: 3, maxRows: 5}}
                    />
                </Form.Item>
            </Col>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Информация, необходимая для перевода денежных средств
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="electronicAccountNumber"
                        label="Номер электронного счета"
                    >
                        <Input placeholder="Введите номер электронного счета"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="dateOfFormationOfElectronicAccount"
                               label="Дата формирования электронного счета">
                        <DatePicker placeholder="Введите дату формирования счета"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="indicationOfNDSInclusionInAmount" label="Признак включения НДС в сумму">
                        <Input placeholder="Введите признак"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="NDSRateAsPercentage" label="Ставка НДС, %">
                        <Input placeholder="Введите ставку НДС в %"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="amountOfNDS" label="Сумма НДС">
                        <Input placeholder="Введите сумму НДС"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="invoiceAmount" label="Сумма счета">
                        <Input placeholder="Введите сумму счета"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="amountToBePaid" label="Сумма к оплате">
                        <Input placeholder="Введите сумму к оплате"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="currencyCodeOfAmount" label="Код валюты суммы">
                        <Input placeholder="Введите код валюты"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="purposeOfPayment" label="Назначение платежа">
                        <Input placeholder="Введите назначение платежа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="validityPeriod" label="Срок действия">
                        <DatePicker placeholder="Введите срок действия"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты получателя средств
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="nameOfRecipient" label="Наименование получателя">
                        <Input placeholder="Введите получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="innOfRecipient" label="ИНН получателя">
                        <Input placeholder="Введите ИНН получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="kppOfRecipient" label="КПП получателя">
                        <Input placeholder="Введите КПП получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="recipientsAccountNumber"
                               label="Номер счета получателя">
                        <Input placeholder="Введите номер счета получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="nameofRecipientsBank"
                               label="Наименование банка получателя">
                        <Input placeholder="Введите банк получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="bicOfRecipientsBank" label="БИК банка получателя">
                        <Input placeholder="Введите БИК банка получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="correspondentAccountOfRecipientsBank"
                        label="Корреспондентский счет банка получателя"
                    >
                        <Input placeholder="Введите корр. счет банка получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты организации плательщика
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="nameOfPayersBank"
                               label="Наименование банка плательщика">
                        <Input placeholder="Введите банк плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="innOfPayersBank" label="ИНН банка плательщика">
                        <Input placeholder="Введите ИНН банка плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="kppOfPayersBank" label="КПП банка плательщика">
                        <Input placeholder="Введите КПП банка плательщика"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты ответа
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="responseId" label="Идентификатор ответа">
                        <Input placeholder="Введите идентификатор ответа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="codeOfProcessingResult"
                               label="Код результата обработки">
                        <Input placeholder="Введите код результата обработки"/>
                    </Form.Item>
                </Col>
            </Row>


            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Сведения об ошибке
            </Divider>

            <Col span={8}>
                <Form.Item style={{marginRight: '50px'}} name="errorCode" label="Код ошибки">
                    <Input placeholder="Введите код"/>
                </Form.Item>
            </Col>

            <Col span={16}>
                <Form.Item style={{marginRight: '50px'}} name="errorDescription" label="Описание ошибки">
                    <TextArea
                        placeholder="Введите описание"
                        autoSize={{minRows: 3, maxRows: 5}}
                    />
                </Form.Item>
            </Col>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Поля сертификата УКЭП
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="idOfQualificationCertificate"
                               label="Уникальный номер квалифицированного сертификата">
                        <Input placeholder="Введите номер сертификата"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="dateOfStartOfQualifiedCertificate"
                               label="Дата начала действия квалифицированного сертификата">
                        <DatePicker placeholder="Введите дату начала действия"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfEndOfQualifiedCertificate"
                               label="Дата окончания действия квалифицированного сертификата">
                        <DatePicker placeholder="Введите дату окончания действия"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="fullNameInQualifiedCertificate" label="ФИО">
                        <Input placeholder="Введите ФИО"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="snilsInQualifiedCertificate"
                               label="СНИЛС">
                        <Input placeholder="Введите СНИЛС"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="innInQualifiedCertificate"
                               label="ИНН">
                        <Input placeholder="Введите ИНН"/>
                    </Form.Item>
                </Col>
            </Row>

            <Form.Item style={{marginTop: '50px'}}>
                <Button htmlType="submit" type="primary">Сохранить</Button>
                <Button onClick={onCancelButtonClick} style={{marginLeft: '20px'}} type="primary" danger>
                    Отмена
                </Button>
            </Form.Item>
        </Form>
    )
}

export default InformationForTransferAndAccrualReceivingForm;
