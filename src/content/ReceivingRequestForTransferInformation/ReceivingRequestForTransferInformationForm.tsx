import React, {useState} from 'react';
import {Button, Form, Input, Select, Row, DatePicker, Col, Divider, Badge, Upload} from 'antd';
import {
    ReceivingRequestForTransferInformationType,
} from "../../types";
import dayjs from 'dayjs';
import {useAppDispatch} from "../../store";
import {
    addReceivingRequestForTransferInformation,
    editReceivingRequestForTransferInformation
} from "../../store/ReceivingRequestForTransferInformation";
import {UploadOutlined} from "@ant-design/icons";

const {TextArea} = Input;

type PropsType = {
    record?: ReceivingRequestForTransferInformationType,
    onSaveButtonClick: () => void;
    onCancelButtonClick: () => void;
}

function ReceivingRequestForTransferInformationForm({
                                                        record,
                                                        onSaveButtonClick,
                                                        onCancelButtonClick
                                                    }: PropsType) {
    const [form] = Form.useForm();
    const statusFields = ['status', 'statusNumber', 'dateOfCreation'];
    const dispatch = useAppDispatch();
    const [isFileUploaded, setIsFileUploaded] = useState<string | undefined>(record?.electronicSignature);

    return (
        <Form
            name="InformingAboutAcceptanceOfOrderForExecution"
            autoComplete="off"
            layout={'vertical'}
            form={form}
            initialValues={{
                documentId: record?.documentId,
                dateOfCreation: record?.dateOfCreation ? dayjs(record?.dateOfCreation) : undefined,
                sourceOfRequest: record?.sourceOfRequest,
                dateOfResponseFormation: record?.dateOfResponseFormation ? dayjs(record.dateOfResponseFormation) : undefined,
                idOfEnvelopeRequestInVIS: record?.idOfEnvelopeRequestInVIS,
                idOfEnvelopeResponseInVIS: record?.idOfEnvelopeResponseInVIS,
                idOfRequest: record?.idOfRequest,
                dateOfRequestFormation: record?.dateOfRequestFormation ? dayjs(record.dateOfRequestFormation) : undefined,
                routingCode: record?.routingCode,
                electronicSignature: record?.electronicSignature,
                decreeNumber: record?.decreeNumber,
                dateOfDecreeCompilation: record?.dateOfDecreeCompilation ? dayjs(record.dateOfDecreeCompilation) : undefined,
                dateOfDebiting: record?.dateOfDebiting ? dayjs(record.dateOfDebiting) : undefined,
                paymentInRequestAmount: record?.paymentInRequestAmount,
                innOfPayersInRequest: record?.innOfPayersInRequest,
                payerAccountNumberInRequest: record?.payerAccountNumberInRequest,
                responseId: record?.responseId,
                codeOfProcessingResultOfRequest: record?.codeOfProcessingResultOfRequest,
                idOfPayment: record?.idOfPayment,
                dateOfReceipt: record?.dateOfReceipt ? dayjs(record.dateOfReceipt) : undefined,
                dateOfDebitingOfResponse: record?.dateOfDebitingOfResponse ? dayjs(record.dateOfDebitingOfResponse) : undefined,
                paymentOfResponseAmount: record?.paymentOfResponseAmount,
                dateOfPlacementInArchive: record?.dateOfPlacementInArchive ? dayjs(record.dateOfPlacementInArchive) : undefined,
                currencyCodeOfAmount: record?.currencyCodeOfAmount,
                purposeOfPayment: record?.purposeOfPayment,
                statusOfPayers: record?.statusOfPayers,
                budgetClassificationCode: record?.budgetClassificationCode,
                oktmoCode: record?.oktmoCode,
                evidenceOfPaymentCode: record?.evidenceOfPaymentCode,
                indicatorOfTaxPeriod: record?.indicatorOfTaxPeriod,
                idOfInformationAboutIndividual: record?.idOfInformationAboutIndividual,
                valueOfDateOfDocumentOfEvidenceOfPayment: record?.valueOfDateOfDocumentOfEvidenceOfPayment,
                codeOfProcessingResultOfResponse: record?.codeOfProcessingResultOfResponse,
                nameOfRecipient: record?.nameOfRecipient,
                innOfRecipient: record?.innOfRecipient,
                kppOfRecipient: record?.kppOfRecipient,
                recipientsAccountNumber: record?.recipientsAccountNumber,
                nameofRecipientsBank: record?.nameofRecipientsBank,
                bicOfRecipientsBank: record?.bicOfRecipientsBank,
                correspondentAccountOfRecipientsBank: record?.correspondentAccountOfRecipientsBank,
                nameOfPayer: record?.nameOfPayer,
                innOfPayer: record?.innOfPayer,
                kppOfPayer: record?.kppOfPayer,
                payerAccountNumber: record?.payerAccountNumber,
                nameOfPayersBank: record?.nameOfPayersBank,
                bikOfPayersBank: record?.bikOfPayersBank,
                correspondentAccountOfPayersBank: record?.correspondentAccountOfPayersBank,
                errorCode: record?.errorCode,
                errorDescription: record?.errorDescription,
            }}
            onFinish={() => {
                if (form.isFieldsValidating()) {
                    return;
                }

                if (record?.dateOfCreation !== undefined) {
                    if (form.getFieldValue('dateOfCreation') !== undefined) {
                        dispatch(editReceivingRequestForTransferInformation({
                            id: record.dateOfCreation,
                            key: "dateOfCreation",
                            value: form.getFieldValue('dateOfCreation').toISOString(),
                        }));
                    }
                    Object.entries(form.getFieldsValue()).filter(([key]) => !statusFields.includes(key))
                        .map(([value]) => {
                            const isDateValue =
                                value === 'dateOfResponseFormation' || value === 'dateOfRequestFormation' ||
                                value === 'dateOfDecreeCompilation' || value === 'dateOfDebiting' ||
                                value === 'dateOfReceipt' || value === 'dateOfDebitingOfResponse' ||
                                value === 'dateOfPlacementInArchive';

                            if (form.isFieldTouched(value)) {
                                dispatch(editReceivingRequestForTransferInformation({
                                    id: form.getFieldValue('dateOfCreation').toISOString(),
                                    key: value,
                                    value: isDateValue ? form.getFieldValue(value).toISOString() : (value === 'electronicSignature' ? form.getFieldValue(value).file.name : form.getFieldValue(value)),
                                }));
                            }
                        });
                } else {
                    dispatch(addReceivingRequestForTransferInformation({
                        ...form.getFieldsValue(),
                        status: 'Новый',
                        statusNumber: 1,
                        dateOfCreation: form.getFieldValue('dateOfCreation').toISOString(),
                        electronicSignature: form.getFieldValue('electronicSignature').file.name,
                    }))
                }
                onSaveButtonClick();
            }}
        >
            <Row>
                <Col span={8}>
                    <Form.Item
                        name="documentId"
                        style={{marginRight: '50px'}}
                        label="Идентификатор документа"
                        rules={[{required: true, message: 'Введите идентификатор документа'}]}
                    >
                        <Input placeholder="Введите идентификатор документа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="dateOfCreation" label="Дата и время создания"
                        rules={[{ required: true, message: 'Введите дату создания' }]}
                    >
                        <DatePicker showTime placeholder="Введите дату создания"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfResponseFormation" label="Дата и время формирования ответа">
                        <DatePicker showTime placeholder="Введите дату формирования ответа"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="sourceOfRequest" label="Источник запроса">
                        <Select placeholder='Выберите источник запроса'>
                            <Select.Option value="local">Локально</Select.Option>
                            <Select.Option value="global">Глобально</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        name="idOfEnvelopeRequestInVIS"
                        style={{marginRight: '50px'}}
                        label="Идентификатор запроса конверта в ВИС"
                    >
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="idOfEnvelopeResponseInVIS"
                               label="Идентификатор ответа конверта в ВИС">
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты запроса
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="idOfRequest"
                               label="Идентификатор запроса">
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="routingCode"
                        label="Код маршрутизации"
                        rules={[{required: true, message: 'Введите код маршрутизации'}]}
                    >
                        <Input placeholder="Введите код маршрутизации"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'Введите дату и время формирования запроса'}]}
                        name="dateOfRequestFormation"
                        label="Дата и время формирования запроса"
                    >
                        <DatePicker showTime placeholder="Введите дату и время формирования запроса"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="decreeNumber"
                        label="Номер распоряжения"
                        rules={[{required: true, message: 'Введите номер распоряжения'}]}
                    >
                        <Input placeholder="Введите номер распоряжения"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="dateOfDecreeCompilation"
                        label="Дата составления распоряжения"
                        rules={[{required: true, message: 'Введите дату составления распоряжения'}]}
                    >
                        <DatePicker placeholder="Введите дату составления распоряжения"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item rules={[{required: true, message: 'Введите дату списания'}]} name="dateOfDebiting"
                               label="Дата списания">
                        <DatePicker placeholder="Введите дату списания"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'Введите сумму платежа'}]}
                        style={{marginRight: '50px'}}
                        name="paymentInRequestAmount"
                        label="Сумма платежа"
                    >
                        <Input placeholder="Введите сумму платежа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'Введите ИНН плательщика'}]}
                        style={{marginRight: '50px'}}
                        name="innOfPayersInRequest" label="ИНН плательщика"
                    >
                        <Input placeholder="Введите ИНН плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'Введите номер счета плательщика'}]}
                        name="payerAccountNumberInRequest" label="Номер счета плательщика"
                    >
                        <Input placeholder="Введите номер счета плательщика"/>
                    </Form.Item>
                </Col>
            </Row>

            <Col span={8}>
                <Form.Item style={{marginRight: '50px'}} name="electronicSignature" label="Электронная подпись">
                    <Upload
                        showUploadList={false}
                        onChange={(file) => {
                            setIsFileUploaded(file.file.name);
                        }}
                    >
                        <Button icon={<UploadOutlined />}>Загрузите файл</Button>
                    </Upload>
                    {isFileUploaded && (
                        <div style={{color: 'green'}}>
                            {isFileUploaded}
                        </div>
                    )}
                </Form.Item>
            </Col>


            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты ответа
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="responseId" label="Идентификатор ответа">
                        <Input placeholder="Введите идентификатор ответа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="codeOfProcessingResultOfRequest"
                               label="Код результата обработки">
                        <Input placeholder="Введите код результата обработки"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="idOfPayment" label="Уникальный номер платежа">
                        <Input placeholder="Введите уникальный номер платежа"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="paymentOfResponseAmount" label="Сумма платежа">
                        <Input placeholder="Введите сумму платежа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="dateOfReceipt" label="Дата поступления">
                        <DatePicker placeholder="Введите дату поступления"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfDebitingOfResponse" label="Дата списания">
                        <DatePicker placeholder="Введите дату списания"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="currencyCodeOfAmount" label="Код валюты суммы">
                        <Input placeholder="Введите код валюты суммы"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="purposeOfPaymentdateOfReceipt"
                               label="Назначение платежа">
                        <Input placeholder="Введите назначение платежа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfPlacementInArchive" label="Дата помещения в картотеку">
                        <DatePicker placeholder="Введите дату помещения в картотеку"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="statusOfPayers"
                               label="Статус лица или органа, составившего распоряжения">
                        <Input placeholder="Введите статус плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="budgetClassificationCode"
                               label="Код бюджетной классификации">
                        <Input placeholder="Введите код классификации"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="oktmoCode" label="Код ОКТМО">
                        <Input placeholder="Введите код ОКТМО"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="evidenceOfPaymentCode" label="Код основания платежа">
                        <Input placeholder="Введите код основания платежа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="indicatorOfTaxPeriod"
                               label="Показатель налогового периода">
                        <Input placeholder="Введите показатель налогового периода"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="codeOfProcessingResultOfResponse"
                               label="Код результата обработки">
                        <Input placeholder="Введите код результата обработки"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="idOfInformationAboutIndividual"
                               label="Идентификатор сведений о физическом лице">
                        <Input placeholder="Введите идентификатор сведений о физическом лице"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="valueOfDateOfDocumentOfEvidenceOfPayment"
                               label="Значение даты документа основания платежа">
                        <Input placeholder="Введите значение даты документа основания платежа"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты получателя средств
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="nameOfRecipient" label="Наименование получателя">
                        <Input placeholder="Введите получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="innOfRecipient" label="ИНН получателя">
                        <Input placeholder="Введите ИНН получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="kppOfRecipient" label="КПП получателя">
                        <Input placeholder="Введите КПП получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="recipientsAccountNumber"
                               label="Номер счета получателя">
                        <Input placeholder="Введите номер счета получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="nameofRecipientsBank"
                               label="Наименование банка получателя">
                        <Input placeholder="Введите банк получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="bicOfRecipientsBank" label="БИК банка получателя">
                        <Input placeholder="Введите БИК банка получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="correspondentAccountOfRecipientsBank"
                        label="Корреспондентский счет банка получателя"
                    >
                        <Input placeholder="Введите корр. счет банка получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты плательщика
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="nameOfPayer"
                               label="Наименование плательщика">
                        <Input placeholder="Введите плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="innOfPayer" label="ИНН плательщика">
                        <Input placeholder="Введите ИНН плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="kppOfPayer" label="КПП плательщика">
                        <Input placeholder="Введите КПП плательщика"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="payerAccountNumber"
                               label="Номер счета плательщика">
                        <Input placeholder="Введите номер счета плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="nameOfPayersBank"
                               label="Наименование банка плательщика">
                        <Input placeholder="Введите банк плательщика"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="bikOfPayersBank" label="БИК банка плательщика">
                        <Input placeholder="Введите БИК банка плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="correspondentAccountOfPayersBank"
                               label="Корр. счет банка плательщика">
                        <Input placeholder="Введите корр. счет банка плательщика"/>
                    </Form.Item>
                </Col>
            </Row>


            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Сведения об ошибке
            </Divider>

            <Col span={8}>
                <Form.Item style={{marginRight: '50px'}} name="errorCode" label="Код ошибки">
                    <Input placeholder="Введите код"/>
                </Form.Item>
            </Col>

            <Col span={16}>
                <Form.Item style={{marginRight: '50px'}} name="errorDescription" label="Описание ошибки">
                    <TextArea
                        placeholder="Введите описание"
                        autoSize={{minRows: 3, maxRows: 5}}
                    />
                </Form.Item>
            </Col>

            <Form.Item style={{marginTop: '50px'}}>
                <Button htmlType="submit" type="primary">Сохранить</Button>
                <Button onClick={onCancelButtonClick} style={{marginLeft: '20px'}} type="primary" danger>
                    Отмена
                </Button>
            </Form.Item>
        </Form>
    )
}

export default ReceivingRequestForTransferInformationForm;
