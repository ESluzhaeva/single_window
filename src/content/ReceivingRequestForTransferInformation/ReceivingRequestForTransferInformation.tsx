import React, {useState} from 'react';
import {Button, Card, Col, Divider, Row, Statistic, Tag, Typography} from 'antd';
import {useAppSelector} from "../../store";
import {useDispatch} from "react-redux";
import {DeleteOutlined, DownOutlined, EditOutlined, PlusOutlined, UpOutlined} from "@ant-design/icons";
import {formatDateToCustomString, sourceOfRequestEnum} from "../../utils";
import ReceivingRequestForTransferInformationForm from "./ReceivingRequestForTransferInformationForm";
import {deleteReceivingRequestForTransferInformation} from "../../store/ReceivingRequestForTransferInformation";

const {Title} = Typography;

function ReceivingRequestForTransferInformation() {
    const arrayWithReceivingRequestForTransferInformation = useAppSelector(
        (state) => state.arrayWithReceivingRequestForTransferInformation.arrayWithReceivingRequestForTransferInformation,
    );
    const [isCreation, setIsCreation] = useState(false);
    const [editingCard, setEditingCard] = useState<string | undefined>(undefined);
    const [isCardExpanded, setIsCardExpanded] = useState<string | undefined>(undefined);
    const dispatch = useDispatch();

    const baseFields = ['status', 'statusNumber', 'documentId', 'dateOfCreation', 'sourceOfRequest', 'dateOfRequestFormation'];

    const additionalFieldsLabels = {
        dateOfResponseFormation: "Дата и время формирования ответа",
        idOfEnvelopeRequestInVIS: "Идентификатор запроса конверта в ВИС",
        idOfEnvelopeResponseInVIS: "Идентификатор ответа конверта в ВИС",
        idOfRequest: "Идентификатор запроса",
        routingCode: "Код маршрутизации",
        decreeNumber: "Номер распоряжения",
        dateOfDecreeCompilation: "Дата составления распоряжения",
        dateOfDebiting: "Дата списания",
        paymentInRequestAmount: "Сумма платежа",
        innOfPayersInRequest: "ИНН плательщика",
        electronicSignature: "Электронная подпись",
        payerAccountNumberInRequest: "Номер счета плательщика",
        responseId: "Идентификатор ответа",
        codeOfProcessingResultOfRequest: "Код результата обработки",
        idOfPayment: "Уникальный номер платежа",
        dateOfReceipt: "Дата поступления",
        dateOfDebitingOfResponse: "Дата списания ответа",
        paymentOfResponseAmount: "Сумма платежа",
        dateOfPlacementInArchive: "Дата помещения в картотеку",
        currencyCodeOfAmount: "Код валюты суммы",
        purposeOfPayment: "Назначение платежа",
        statusOfPayers: "Статус лица или органа, составившего распоряжение",
        budgetClassificationCode: "Код бюджетной классификации",
        oktmoCode: "Код ОКТМО",
        evidenceOfPaymentCode: "Код основания платежа",
        indicatorOfTaxPeriod: "Показатель налогового периода",
        idOfInformationAboutIndividual: "Идентификатор сведений о физическом лице",
        valueOfDateOfDocumentOfEvidenceOfPayment: "Значение даты документа основания платежа",
        codeOfProcessingResultOfResponse: "Код результата обработки",
        nameOfRecipient: "Наименование получателя",
        innOfRecipient: "ИНН получателя",
        kppOfRecipient: "КПП получателя",
        recipientsAccountNumber: "Номер счета получателя",
        nameofRecipientsBank: "Наименование банка получателя",
        bicOfRecipientsBank: "БИК банка получателя",
        correspondentAccountOfRecipientsBank: "Корреспондентский счет банка получателя",
        nameOfPayer: "Наименование плательщика",
        innOfPayer: "ИНН плательщика",
        kppOfPayer: "КПП плательщика",
        payerAccountNumber: "Номер счета плательщика",
        nameOfPayersBank: "Наименование банка плательщика",
        bikOfPayersBank: "БИК банка плательщика",
        correspondentAccountOfPayersBank: "Корреспондентский счет банка плательщика",
        errorCode: "Код ошибки",
        errorDescription: "Описание ошибки",
    }

    return isCreation ? (
        <ReceivingRequestForTransferInformationForm
            onSaveButtonClick={() => setIsCreation(false)}
            onCancelButtonClick={() => setIsCreation(false)}
        />
    ) : ((
        <div>
            {!isCreation && !editingCard && (
                <Row justify='space-between'>
                    <Title style={{margin: '0 0 30px 0'}} level={3}>Получение запроса информации о переводе</Title>
                    <Button onClick={() => setIsCreation(true)} type="primary" icon={<PlusOutlined/>} size="large"/>
                </Row>
            )}

            {editingCard && (
                <ReceivingRequestForTransferInformationForm
                    key={editingCard}
                    record={arrayWithReceivingRequestForTransferInformation.filter((value) => value.dateOfCreation === editingCard)[0]}
                    onSaveButtonClick={() => setEditingCard(undefined)}
                    onCancelButtonClick={() => setEditingCard(undefined)}
                />
            )}

            <Col span={32}>
                {!editingCard && arrayWithReceivingRequestForTransferInformation.map((card) => (
                    <Card style={{marginBottom: '20px'}} key={card.dateOfCreation} title={
                        <Row justify='space-between'>
                            <div>Статус: <Tag color="lime">{card.status}</Tag></div>
                            <div>
                                <Button
                                    style={{marginRight: '10px'}}
                                    onClick={() => setEditingCard(card.dateOfCreation)}
                                    icon={<EditOutlined/>}
                                />
                                <Button
                                    onClick={() => dispatch(deleteReceivingRequestForTransferInformation(card.dateOfCreation))}
                                    icon={<DeleteOutlined/>}
                                />
                            </div>
                        </Row>
                    }>
                        <Row style={{marginBottom: '30px'}} justify={"space-between"}>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Идентификатор документа"
                                       value={card.documentId}/>
                            <Statistic style={{marginRight: '10px'}} title="Дата создания"
                                       value={formatDateToCustomString(card.dateOfCreation, 'dateOfCreation')}/>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Код статуса"
                                       value={card.statusNumber}/>
                            {card.sourceOfRequest && (
                                // @ts-ignore
                                <Statistic value={sourceOfRequestEnum[card.sourceOfRequest]} style={{marginRight: '10px'}}
                                           groupSeparator={''} title="Источник запроса" />)}
                            <Statistic
                                style={{marginRight: '10px'}}
                                title="Дата и время формирования запроса"
                                value={formatDateToCustomString(card.dateOfRequestFormation, 'dateOfRequestFormation')}
                            />
                        </Row>

                        <Divider/>

                        <div style={{display: 'grid', gridTemplateColumns: 'repeat(4, 1fr)', gap: '16px'}}>
                            {isCardExpanded === card.dateOfCreation && Object.entries(card).filter(([key]) => !baseFields.includes(key))
                                .map(([key, value]) => (
                                        (value !== undefined) && (
                                            (key === 'dateOfResponseFormation' || key === 'dateOfDecreeCompilation' ||
                                                key === 'dateOfDebiting' || key === 'dateOfReceipt' || key === 'dateOfDebitingOfResponse' ||
                                                key === 'dateOfPlacementInArchive') ? (
                                                <div key={key}>
                                                    {/*@ts-ignore*/}
                                                    <Statistic groupSeparator={''} title={additionalFieldsLabels[key]} value={formatDateToCustomString(value, key)}
                                                    />
                                                </div>
                                            ) : (
                                                <div key={key}>
                                                    {/*@ts-ignore*/}
                                                    <Statistic groupSeparator={''} title={additionalFieldsLabels[key]} value={value}/>
                                                </div>
                                            )
                                        )
                                    )
                                )
                            }
                        </div>

                        <Row style={isCardExpanded === card.dateOfCreation ? {marginTop: '20px'} : undefined}>
                            <Button
                                onClick={() => setIsCardExpanded((prev) => prev === undefined ? card.dateOfCreation : undefined)}
                                icon={isCardExpanded !== card.dateOfCreation ? <DownOutlined/> : <UpOutlined/>}
                                size="middle"
                            >
                                Доп. данные
                            </Button>
                        </Row>
                    </Card>
                ))}
            </Col>
        </div>
    ))
}

export default ReceivingRequestForTransferInformation;
