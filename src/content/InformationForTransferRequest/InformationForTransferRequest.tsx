import React, {useState} from 'react';
import {Button, Card, Col, Divider, Row, Statistic, Tag, Typography} from 'antd';
import {useAppSelector} from "../../store";
import {DeleteOutlined, DownOutlined, EditOutlined, PlusOutlined, UpOutlined} from "@ant-design/icons";
import {deleteInformationForTransferRequest} from "../../store/InformationForTransferRequest";
import {formatDateToCustomString, sourceOfRequestEnum} from "../../utils";
import {useDispatch} from "react-redux";
import InformationForTransferRequestForm from "./InformationForTransferRequestForm";

const {Title} = Typography;

function InformationForTransferRequest() {
    const informationForTransferRequests = useAppSelector(
        (state) => state.informationForTransferRequests.informationForTransferRequests,
    );
    const [isCreation, setIsCreation] = useState(false);
    const [editingCard, setEditingCard] = useState<string | undefined>(undefined);
    const [isCardExpanded, setIsCardExpanded] = useState<string | undefined>(undefined);
    const dispatch = useDispatch();

    const requiredFields = ['status', 'statusNumber', 'documentId', 'dateOfCreation', 'sourceOfRequest', 'idOfRequest', 'idOfUniquePayment'];

    const additionalFieldsLabels = {
        dateOfDispatch: "Дата и время отправки",
        idOfEnvelopeRequestInVIS: "Идентификатор запроса конверта в ВИС",
        idOfEnvelopeResponseInVIS: "Идентификатор ответа конверта в ВИС",
        responseId: "Идентификатор ответа",
        codeOfProcessingResult: "Код результата обработки",
        dateOfResponseFormation: "Дата и время формирования ответа",
        descriptionOfProcessingResult: "Описание результата обработки",
        dateOfFormationOfElectronicAccount: "Дата формирования электронного счета",
        indicationOfNDSInclusionInAmount: "Признак включения НДС в сумму",
        NDSRateAsPercentage: "Ставка НДС, %",
        amountOfNDS: "Сумма НДС",
        invoiceAmount: "Сумма счета",
        amountToBePaid: "Сумма к оплате",
        currencyCodeOfAmount: "Код валюты суммы",
        purposeOfPayment: "Назначение платежа",
        validityPeriod: "Срок действия",
        nameOfRecipient: "Наименование получателя",
        innOfRecipient: "ИНН получателя",
        kppOfRecipient: "КПП получателя",
        recipientsAccountNumber: "Номер счета получателя",
        nameofBank: "Наименование банка",
        bicOfRecipientsBank: "БИК банка получателя",
        correspondentAccountOfRecipientsBank: "Корреспондентский счет банка получателя",
        nameOfPayersBank: "Наименование банка плательщика",
        innOfPayersBank: "ИНН банка плательщика",
        kppOfPayersBank: "КПП банка плательщика",
        errorCode: "Код ошибки",
        errorDescription: "Описание ошибки"
    }

    return isCreation ? (
        <InformationForTransferRequestForm
            onSaveButtonClick={() => setIsCreation(false)}
            onCancelButtonClick={() => setIsCreation(false)}
        />
    ) : ((
        <div>
            {!isCreation && !editingCard && (
                <Row justify='space-between'>
                    <Title style={{margin: '0 0 30px 0'}} level={3}>Запрос информации для перевода</Title>
                    <Button onClick={() => setIsCreation(true)} type="primary" icon={<PlusOutlined/>} size="large"/>
                </Row>
            )}

            {editingCard && (
                <InformationForTransferRequestForm
                    key={editingCard}
                    record={informationForTransferRequests.filter((value) => value.dateOfCreation === editingCard)[0]}
                    onSaveButtonClick={() => setEditingCard(undefined)}
                    onCancelButtonClick={() => setEditingCard(undefined)}
                />
            )}

            <Col span={32}>
                {!editingCard && informationForTransferRequests.map((card) => (
                    <Card style={{marginBottom: '20px'}} key={card.dateOfCreation} title={
                        <Row justify='space-between'>
                            <div>Статус: <Tag color="lime">{card.status}</Tag></div>
                            <div>
                                <Button
                                    style={{marginRight: '10px'}}
                                    onClick={() => setEditingCard(card.dateOfCreation)}
                                    icon={<EditOutlined/>}
                                />
                                <Button
                                    onClick={() => dispatch(deleteInformationForTransferRequest(card.dateOfCreation))}
                                    icon={<DeleteOutlined/>}
                                />
                            </div>
                        </Row>
                    }>
                        <Row style={{marginBottom: '30px'}} justify={"space-between"}>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Идентификатор документа"
                                       value={card.documentId}/>
                            <Statistic style={{marginRight: '10px'}} title="Дата создания"
                                       value={formatDateToCustomString(card.dateOfCreation, 'dateOfCreation')}/>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Код статуса"
                                       value={card.statusNumber}/>
                            {/*@ts-ignore*/}
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Источник запроса" value={sourceOfRequestEnum[card.sourceOfRequest]}/>
                            {card.idOfRequest && (<Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Идентификатор запроса"
                                        value={card.idOfRequest}/>)}
                            <Statistic groupSeparator={''} title="Номер электронного счета"
                                       value={card.idOfUniquePayment}/>
                        </Row>

                        <Divider/>

                        <div style={{display: 'grid', gridTemplateColumns: 'repeat(4, 1fr)', gap: '16px'}}>
                            {isCardExpanded === card.dateOfCreation && Object.entries(card).filter(([key]) => !requiredFields.includes(key))
                                .map(([key, value]) => (
                                    (value !== undefined) && (
                                        (key === 'dateOfDispatch' || key === 'dateOfResponseFormation' || key === 'dateOfFormationOfElectronicAccount' || key === 'validityPeriod') ? (
                                            <div key={key}>
                                                {/*@ts-ignore*/}
                                                <Statistic groupSeparator={''} title={additionalFieldsLabels[key]}
                                                           value={formatDateToCustomString(value, key)}
                                                />
                                            </div>
                                        ) : (
                                            <div key={key}>
                                                {/*@ts-ignore*/}
                                                <Statistic groupSeparator={''} title={additionalFieldsLabels[key]}
                                                           value={value}/>
                                            </div>
                                        )
                                    )
                                ))
                            }
                        </div>

                        <Row style={isCardExpanded === card.dateOfCreation ? {marginTop: '20px'} : undefined}>
                            <Button
                                onClick={() => setIsCardExpanded((prev) => prev === undefined ? card.dateOfCreation : undefined)}
                                icon={isCardExpanded !== card.dateOfCreation ? <DownOutlined/> : <UpOutlined/>}
                                size="middle"
                            >
                                Доп. данные
                            </Button>
                        </Row>
                    </Card>
                ))}
            </Col>
        </div>
    ))
}

export default InformationForTransferRequest;
