import React from 'react';
import {Button, Form, Input, Select, Row, DatePicker, Col, Divider, Badge} from 'antd';
import {InformationForTransferRequestType} from "../../types";
import dayjs from 'dayjs';
import {useAppDispatch} from "../../store";
import {addInformationForTransferRequest, editInformationForTransferRequest} from "../../store/InformationForTransferRequest";

const { TextArea } = Input;

type PropsType = {
    record?: InformationForTransferRequestType,
    onSaveButtonClick: () => void;
    onCancelButtonClick: () => void;
}

function InformationForTransferRequestForm({record, onSaveButtonClick, onCancelButtonClick}: PropsType) {
    const [form] = Form.useForm();
    const statusFields = ['status', 'statusNumber', 'dateOfCreation'];
    const dispatch = useAppDispatch();

    return (
        <Form
            name="InformationForTransferRequestForm"
            autoComplete="off"
            layout={'vertical'}
            form={form}
            initialValues={{
                documentId: record?.documentId,
                dateOfDispatch: record?.dateOfDispatch ? dayjs(record?.dateOfDispatch): undefined,
                dateOfCreation: record?.dateOfCreation ? dayjs(record?.dateOfCreation) : undefined,
                sourceOfRequest: record?.sourceOfRequest,
                idOfRequest: record?.idOfRequest,
                idOfEnvelopeRequestInVIS: record?.idOfEnvelopeRequestInVIS,
                idOfEnvelopeResponseInVIS: record?.idOfEnvelopeResponseInVIS,
                idOfUniquePayment: record?.idOfUniquePayment,
                responseId: record?.responseId,
                codeOfProcessingResult: record?.codeOfProcessingResult,
                dateOfResponseFormation: record?.dateOfResponseFormation ? dayjs(record?.dateOfResponseFormation): undefined,
                descriptionOfProcessingResult: record?.descriptionOfProcessingResult,
                errorCode: record?.errorCode,
                errorDescription: record?.errorDescription,
                dateOfFormationOfElectronicAccount: record?.dateOfFormationOfElectronicAccount ? dayjs(record?.dateOfFormationOfElectronicAccount) : undefined,
                indicationOfNDSInclusionInAmount: record?.indicationOfNDSInclusionInAmount,
                NDSRateAsPercentage: record?.NDSRateAsPercentage,
                amountOfNDS: record?.amountOfNDS,
                invoiceAmount: record?.invoiceAmount,
                amountToBePaid: record?.amountToBePaid,
                currencyCodeOfAmount: record?.currencyCodeOfAmount,
                purposeOfPayment: record?.purposeOfPayment,
                validityPeriod: record?.validityPeriod ? dayjs(record?.validityPeriod) : undefined,
                nameOfRecipient: record?.nameOfRecipient,
                innOfRecipient: record?.innOfRecipient,
                kppOfRecipient: record?.kppOfRecipient,
                recipientsAccountNumber: record?.recipientsAccountNumber,
                nameofBank: record?.nameofBank,
                bicOfRecipientsBank: record?.bicOfRecipientsBank,
                correspondentAccountOfRecipientsBank: record?.correspondentAccountOfRecipientsBank,
                nameOfPayersBank: record?.nameOfPayersBank,
                innOfPayersBank: record?.innOfPayersBank,
                kppOfPayersBank: record?.kppOfPayersBank,
            }}
            onFinish={() => {

                if (form.isFieldsValidating()) {
                    return;
                }

                if (record?.dateOfCreation !== undefined) {
                    if (form.getFieldValue('dateOfCreation') !== undefined) {
                        dispatch(editInformationForTransferRequest({
                            id: record.dateOfCreation,
                            key: "dateOfCreation",
                            value: form.getFieldValue('dateOfCreation').toISOString(),
                        }));
                    }
                    Object.entries(form.getFieldsValue()).filter(([key]) => !statusFields.includes(key))
                        .map(([value]) => {
                            const isDateValue = value === 'dateOfDispatch' || value === 'dateOfResponseFormation' || value === 'dateOfFormationOfElectronicAccount' || value === 'validityPeriod';
                            if (form.isFieldTouched(value)) {
                                dispatch(editInformationForTransferRequest({
                                    id: form.getFieldValue('dateOfCreation').toISOString(),
                                    key: value,
                                    value: isDateValue ? form.getFieldValue(value).toISOString() : form.getFieldValue(value),
                                }));
                            }
                        });
                } else {
                    dispatch(addInformationForTransferRequest({
                        ...form.getFieldsValue(),
                        status: 'Новый',
                        statusNumber: 1,
                        dateOfCreation: form.getFieldValue('dateOfCreation').toISOString(),
                    }))
                }
                onSaveButtonClick();
            }}
        >
            <Row>
                <Form.Item
                    name="documentId"
                    style={{width: '350px', marginRight: '50px'}}
                    label="Идентификатор документа"
                    rules={[{ required: true, message: 'Введите идентификатор документа' }]}
                >
                    <Input placeholder="Введите идентификатор документа"/>
                </Form.Item>
                <Form.Item
                    rules={[{ required: true, message: 'Введите дату создания' }]}
                    name="dateOfCreation" label="Дата и время создания"
                >
                    <DatePicker showTime placeholder="Введите дату создания" />
                </Form.Item>
            </Row>
            <Row>
                <Form.Item style={{marginRight: '50px'}} name="dateOfDispatch" label="Дата и время отправки">
                    <DatePicker showTime placeholder="Введите дату отправки" />
                </Form.Item>
                <Form.Item
                    rules={[{ required: true, message: 'Выберите источник запроса' }]}
                    name="sourceOfRequest"
                    label="Источник запроса"
                >
                    <Select placeholder='Выберите источник запроса'>
                        <Select.Option value="local">Локально</Select.Option>
                        <Select.Option value="global">Глобально</Select.Option>
                    </Select>
                </Form.Item>
            </Row>
            <Row>
                <Form.Item
                    name="idOfEnvelopeRequestInVIS"
                    style={{marginRight: '50px'}}
                    label="Идентификатор запроса конверта в ВИС"
                >
                    <Input placeholder="Введите идентификатор запроса"/>
                </Form.Item>
                <Form.Item name="idOfEnvelopeResponseInVIS" label="Идентификатор ответа конверта в ВИС">
                    <Input placeholder="Введите идентификатор запроса" />
                </Form.Item>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  " />
                Реквизиты запроса
            </Divider>

            <Row>
                <Col span={16}>
                    <Form.Item name="idOfRequest" label="Идентификатор запроса">
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
                <Col span={16}>
                    <Form.Item rules={[{ required: true, message: 'Введите номер электронного счета' }]} name="idOfUniquePayment" label="Номер электронного счета">
                        <Input placeholder="Введите номер электронного счета"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  " />
                Реквизиты ответа
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item name="responseId" label="Идентификатор ответа">
                        <Input placeholder="Введите идентификатор ответа"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="codeOfProcessingResult" label="Код результата обработки">
                        <Input placeholder="Введите код"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfResponseFormation" label="Дата и время формирования ответа">
                        <DatePicker showTime placeholder="Введите дату формирования ответа" />
                    </Form.Item>
                </Col>
            </Row>

            <Col span={16}>
                <Form.Item name="descriptionOfProcessingResult" label="Описание результата обработки">
                    <TextArea
                        placeholder="Введите описание"
                        autoSize={{ minRows: 3, maxRows: 5 }}
                    />
                </Form.Item>
            </Col>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  " />
                Реквизиты для перевода средств
            </Divider>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="dateOfFormationOfElectronicAccount" label="Дата формирования электронного счета">
                        <DatePicker placeholder="Введите дату формирования счета" />
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="indicationOfNDSInclusionInAmount" label="Признак включения НДС в сумму">
                        <Input placeholder="Введите признак"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="NDSRateAsPercentage" label="Ставка НДС, %">
                        <Input placeholder="Введите ставку НДС в %"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="amountOfNDS" label="Сумма НДС">
                        <Input placeholder="Введите сумму НДС"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="invoiceAmount" label="Сумма счета">
                        <Input placeholder="Введите сумму счета"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="amountToBePaid" label="Сумма к оплате">
                        <Input placeholder="Введите сумму к оплате"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="currencyCodeOfAmount" label="Код валюты суммы">
                        <Input placeholder="Введите код валюты"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="purposeOfPayment" label="Назначение платежа">
                        <Input placeholder="Введите назначение платежа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="validityPeriod" label="Срок действия">
                        <DatePicker placeholder="Введите срок действия" />
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  " />
                Реквизиты получателя средств
            </Divider>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="nameOfRecipient" label="Наименование получателя">
                        <Input placeholder="Введите получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="innOfRecipient" label="ИНН получателя">
                        <Input placeholder="Введите ИНН получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="kppOfRecipient" label="КПП получателя">
                        <Input placeholder="Введите КПП получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="recipientsAccountNumber" label="Номер счета получателя">
                        <Input placeholder="Введите номер счета получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="nameofBank" label="Наименование банка">
                        <Input placeholder="Введите банк"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="bicOfRecipientsBank" label="БИК банка получателя">
                        <Input placeholder="Введите БИК банка получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item name="correspondentAccountOfRecipientsBank" label="Корреспондентский счет банка получателя">
                        <Input placeholder="Введите корр. счет банка получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  " />
                Реквизиты организации плательщика
            </Divider>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="nameOfPayersBank" label="Наименование банка плательщика">
                        <Input placeholder="Введите банк плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="innOfPayersBank" label="ИНН банка плательщика">
                        <Input placeholder="Введите ИНН банка плательщика"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item name="kppOfPayersBank" label="КПП банка плательщика">
                        <Input placeholder="Введите КПП банка плательщика"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  " />
                Сведения об ошибке
            </Divider>

            <Col span={8}>
                <Form.Item name="errorCode" label="Код ошибки">
                    <Input placeholder="Введите код"/>
                </Form.Item>
            </Col>

            <Col span={16}>
                <Form.Item name="errorDescription" label="Описание ошибки">
                    <TextArea
                        placeholder="Введите описание"
                        autoSize={{ minRows: 3, maxRows: 5 }}
                    />
                </Form.Item>
            </Col>

            <Form.Item style={{marginTop: '50px'}}>
                <Button htmlType="submit" type="primary">Сохранить</Button>
                <Button onClick={onCancelButtonClick} style={{marginLeft: '20px'}} type="primary" danger>
                    Отмена
                </Button>
            </Form.Item>
        </Form>
    )
}

export default InformationForTransferRequestForm;
