import React from 'react';
import {Button, Form, Input, Select, Row, DatePicker, Col, Divider, Badge} from 'antd';
import {
    InformingAboutAcceptanceOfOrderForExecutionType,
} from "../../types";
import dayjs from 'dayjs';
import {useAppDispatch} from "../../store";
import {
    addInformingAboutAcceptanceOfOrderForExecution,
    editInformingAboutAcceptanceOfOrderForExecution
} from "../../store/InformingAboutAcceptanceOfOrderForExecution";

const {TextArea} = Input;

type PropsType = {
    record?: InformingAboutAcceptanceOfOrderForExecutionType,
    onSaveButtonClick: () => void;
    onCancelButtonClick: () => void;
}

function InformingAboutAcceptanceOfOrderForExecutionForm({
                                                           record,
                                                           onSaveButtonClick,
                                                           onCancelButtonClick
                                                       }: PropsType) {
    const [form] = Form.useForm();
    const statusFields = ['status', 'statusNumber', 'dateOfCreation'];
    const dispatch = useAppDispatch();

    return (
        <Form
            name="InformingAboutAcceptanceOfOrderForExecution"
            autoComplete="off"
            layout={'vertical'}
            form={form}
            initialValues={{
                documentId: record?.documentId,
                dateOfCreation: record?.dateOfCreation ? dayjs(record?.dateOfCreation) : undefined,
                dateOfDispatch: record?.dateOfDispatch ? dayjs(record?.dateOfDispatch) : undefined,
                sourceOfRequest: record?.sourceOfRequest,
                idOfEnvelopeRequestInVIS: record?.idOfEnvelopeRequestInVIS,
                idOfEnvelopeResponseInVIS: record?.idOfEnvelopeResponseInVIS,
                idOfRequest: record?.idOfRequest,
                idOfOperation: record?.idOfOperation,
                electronicAccountNumber: record?.electronicAccountNumber,
                dateOfAcceptanceOfOrderForExecution: record?.dateOfAcceptanceOfOrderForExecution ? dayjs(record.dateOfAcceptanceOfOrderForExecution) : undefined,
                paymentAmount: record?.paymentAmount,
                currencyCodeOfAmount: record?.currencyCodeOfAmount,
                purposeOfPayment: record?.purposeOfPayment,
                reasonForReferral: record?.reasonForReferral,
                dateOfClarificationOfPreviouslySubmittedInformation: record?.dateOfClarificationOfPreviouslySubmittedInformation ? dayjs(record.dateOfClarificationOfPreviouslySubmittedInformation) : undefined,
                nameOfRecipient: record?.nameOfRecipient,
                innOfRecipient: record?.innOfRecipient,
                kppOfRecipient: record?.kppOfRecipient,
                recipientsAccountNumber: record?.recipientsAccountNumber,
                nameofRecipientsBank: record?.nameofRecipientsBank,
                bicOfRecipientsBank: record?.bicOfRecipientsBank,
                correspondentAccountOfRecipientsBank: record?.correspondentAccountOfRecipientsBank,
                nameOfPayer: record?.nameOfPayer,
                innOfPayer: record?.innOfPayer,
                kppOfPayer: record?.kppOfPayer,
                payerAccountNumber: record?.payerAccountNumber,
                nameOfPayersBank: record?.nameOfPayersBank,
                bikOfPayersBank: record?.bikOfPayersBank,
                correspondentAccountOfPayersBank: record?.correspondentAccountOfPayersBank,
                responseId: record?.responseId,
                codeOfProcessingResult: record?.codeOfProcessingResult,
                dateOfResponseFormation: record?.dateOfResponseFormation ? dayjs(record?.dateOfResponseFormation) : undefined,
                descriptionOfProcessingResult: record?.descriptionOfProcessingResult,
                errorCode: record?.errorCode,
                errorDescription: record?.errorDescription,
            }}
            onFinish={() => {
                if (form.isFieldsValidating()) {
                    return;
                }

                if (record?.dateOfCreation !== undefined) {
                    if (form.getFieldValue('dateOfCreation') !== undefined) {
                        dispatch(editInformingAboutAcceptanceOfOrderForExecution({
                            id: record.dateOfCreation,
                            key: "dateOfCreation",
                            value: form.getFieldValue('dateOfCreation').toISOString(),
                        }));
                    }
                    Object.entries(form.getFieldsValue()).filter(([key]) => !statusFields.includes(key))
                        .map(([value]) => {
                            const isDateValue =
                                value === 'dateOfDispatch' ||  value === 'dateOfAcceptanceOfOrderForExecution' ||
                                value === 'dateOfClarificationOfPreviouslySubmittedInformation' || value === 'dateOfResponseFormation';

                            if (form.isFieldTouched(value)) {
                                dispatch(editInformingAboutAcceptanceOfOrderForExecution({
                                    id: form.getFieldValue('dateOfCreation').toISOString(),
                                    key: value,
                                    value: isDateValue ? form.getFieldValue(value).toISOString() : form.getFieldValue(value),
                                }));
                            }
                        });
                } else {
                    dispatch(addInformingAboutAcceptanceOfOrderForExecution({
                        ...form.getFieldsValue(),
                        status: 'Новый',
                        statusNumber: 1,
                        dateOfCreation: form.getFieldValue('dateOfCreation').toISOString(),
                    }))
                }
                onSaveButtonClick();
            }}
        >
            <Row>
                <Col span={8}>
                    <Form.Item
                        name="documentId"
                        style={{marginRight: '50px'}}
                        label="Идентификатор документа"
                        rules={[{required: true, message: 'Введите идентификатор документа'}]}
                    >
                        <Input placeholder="Введите идентификатор документа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="dateOfCreation" label="Дата и время создания"
                        rules={[{ required: true, message: 'Введите дату создания' }]}
                    >
                        <DatePicker showTime placeholder="Введите дату создания"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfDispatch" label="Дата и время отправки">
                        <DatePicker showTime placeholder="Введите дату и время отправки"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="sourceOfRequest"
                        label="Источник запроса"
                        rules={[{required: true, message: 'Выберите источник запроса'}]}
                    >
                        <Select placeholder='Выберите источник запроса'>
                            <Select.Option value="local">Локально</Select.Option>
                            <Select.Option value="global">Глобально</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        name="idOfEnvelopeRequestInVIS"
                        style={{marginRight: '50px'}}
                        label="Идентификатор запроса конверта в ВИС"
                    >
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="idOfEnvelopeResponseInVIS"
                               label="Идентификатор ответа конверта в ВИС">
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты запроса
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        rules={[{required: true, message: 'Введите идентификатор запроса'}]}
                        name="idOfRequest"
                        label="Идентификатор запроса"
                    >
                        <Input placeholder="Введите идентификатор запроса"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        rules={[{required: true, message: 'Введите идентификатор операции'}]}
                        name="idOfOperation"
                        label="Идентификатор операции"
                    >
                        <Input placeholder="Введите идентификатор операции"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'Введите номер электронного счета'}]}
                        name="electronicAccountNumber"
                        label="Номер электронного счета"
                    >
                        <Input placeholder="Введите номер электронного счета"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="paymentAmount"
                        label="Сумма платежа"
                        rules={[{required: true, message: 'Введите сумма платежа'}]}
                    >
                        <Input placeholder="Введите сумму платежа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'Введите код валюты суммы'}]}
                        style={{marginRight: '50px'}}
                        name="currencyCodeOfAmount"
                        label="Код валюты суммы"
                    >
                        <Input placeholder="Введите код валюты"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        name="dateOfAcceptanceOfOrderForExecution"
                        label="Дата приема к исполнению распоряжения"
                        rules={[{required: true, message: 'Введите дату приема к исполнению распоряжения'}]}
                    >
                        <DatePicker placeholder="Введите дату приема к исполнению распоряжения"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="purposeOfPayment"
                        label="Назначение платежа"
                        rules={[{required: true, message: 'Введите назначение платежа'}]}
                    >
                        <Input placeholder="Введите назначение платежа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="reasonForReferral"
                        label="Причина напраления"
                        rules={[{required: true, message: 'Введите причину напраления'}]}
                    >
                        <Input placeholder="Введите причину напраления"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfClarificationOfPreviouslySubmittedInformation" label="Дата уточнения ранее предосталенной информации">
                        <DatePicker placeholder="Введите дату уточнения информации"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты получателя средств
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="nameOfRecipient"
                        label="Наименование получателя"
                        rules={[{required: true, message: 'Введите получателя'}]}
                    >
                        <Input placeholder="Введите получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'ИНН получателя'}]}
                        style={{marginRight: '50px'}}
                        name="innOfRecipient"
                        label="ИНН получателя">
                        <Input placeholder="Введите ИНН получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="kppOfRecipient" label="КПП получателя">
                        <Input placeholder="Введите КПП получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'Введите номер счета получателя'}]}
                        style={{marginRight: '50px'}}
                        name="recipientsAccountNumber"
                        label="Номер счета получателя"
                    >
                        <Input placeholder="Введите номер счета получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="nameofRecipientsBank"
                               label="Наименование банка получателя">
                        <Input placeholder="Введите банк получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="bicOfRecipientsBank"
                        label="БИК банка получателя"
                        rules={[{required: true, message: 'Введите БИК банка получателя'}]}
                    >
                        <Input placeholder="Введите БИК банка получателя"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="correspondentAccountOfRecipientsBank"
                        label="Корреспондентский счет банка получателя"
                    >
                        <Input placeholder="Введите корр. счет банка получателя"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты плательщика
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="nameOfPayer"
                        label="Наименование плательщика"
                        rules={[{required: true, message: 'Введите плательщика'}]}
                    >
                        <Input placeholder="Введите плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="innOfPayer"
                        label="ИНН плательщика"
                        rules={[{required: true, message: 'Введите ИНН плательщика'}]}
                    >
                        <Input placeholder="Введите ИНН плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="kppOfPayer" label="КПП плательщика">
                        <Input placeholder="Введите КПП плательщика"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        style={{marginRight: '50px'}}
                        name="payerAccountNumber"
                        label="Номер счета плательщика"
                        rules={[{required: true, message: 'Введите номер счета плательщика'}]}
                    >
                        <Input placeholder="Введите номер счета плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="nameOfPayersBank"
                               label="Наименование банка плательщика">
                        <Input placeholder="Введите банк плательщика"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col span={8}>
                    <Form.Item
                        rules={[{required: true, message: 'Введите БИК банка плательщика'}]}
                        style={{marginRight: '50px'}}
                        name="bikOfPayersBank"
                        label="БИК банка плательщика">
                        <Input placeholder="Введите БИК банка плательщика"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="correspondentAccountOfPayersBank" label="Корр. счет банка плательщика">
                        <Input placeholder="Введите корр. счет банка плательщика"/>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Реквизиты ответа
            </Divider>

            <Row>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="responseId" label="Идентификатор ответа">
                        <Input placeholder="Введите идентификатор ответа"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item style={{marginRight: '50px'}} name="codeOfProcessingResult"
                               label="Код результата обработки">
                        <Input placeholder="Введите код результата обработки"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfResponseFormation" label="Дата и время формирования ответа">
                        <DatePicker showTime placeholder="Введите дату и время формирования ответа"/>
                    </Form.Item>
                </Col>
            </Row>

            <Col span={16}>
                <Form.Item style={{marginRight: '50px'}} name="descriptionOfProcessingResult" label="Описание результата обработки">
                    <TextArea
                        placeholder="Введите описание"
                        autoSize={{minRows: 3, maxRows: 5}}
                    />
                </Form.Item>
            </Col>


            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  "/>
                Сведения об ошибке
            </Divider>

            <Col span={8}>
                <Form.Item style={{marginRight: '50px'}} name="errorCode" label="Код ошибки">
                    <Input placeholder="Введите код"/>
                </Form.Item>
            </Col>

            <Col span={16}>
                <Form.Item style={{marginRight: '50px'}} name="errorDescription" label="Описание ошибки">
                    <TextArea
                        placeholder="Введите описание"
                        autoSize={{minRows: 3, maxRows: 5}}
                    />
                </Form.Item>
            </Col>

            <Form.Item style={{marginTop: '50px'}}>
                <Button htmlType="submit" type="primary">Сохранить</Button>
                <Button onClick={onCancelButtonClick} style={{marginLeft: '20px'}} type="primary" danger>
                    Отмена
                </Button>
            </Form.Item>
        </Form>
    )
}

export default InformingAboutAcceptanceOfOrderForExecutionForm;
