import React, {useState} from 'react';
import {Button, Card, Col, Divider, Row, Statistic, Tag, Typography} from 'antd';
import {useAppSelector} from "../../store";
import {useDispatch} from "react-redux";
import {DeleteOutlined, DownOutlined, EditOutlined, PlusOutlined, UpOutlined} from "@ant-design/icons";
import {formatDateToCustomString, sourceOfRequestEnum} from "../../utils";
import InformingAboutAcceptanceOfOrderForExecutionForm from "./InformingAboutAcceptanceOfOrderForExecutionForm";
import {
    deleteInformingAboutAcceptanceOfOrderForExecution
} from "../../store/InformingAboutAcceptanceOfOrderForExecution";

const {Title} = Typography;

function InformingAboutAcceptanceOfOrderForExecution() {
    const arrayWithInformingAboutAcceptanceOfOrderForExecution = useAppSelector(
        (state) => state.arrayWithInformingAboutAcceptanceOfOrderForExecution.arrayWithInformingAboutAcceptanceOfOrderForExecution,
    );
    const [isCreation, setIsCreation] = useState(false);
    const [editingCard, setEditingCard] = useState<string | undefined>(undefined);
    const [isCardExpanded, setIsCardExpanded] = useState<string | undefined>(undefined);
    const dispatch = useDispatch();

    const baseFields = ['status', 'statusNumber', 'documentId', 'dateOfCreation', 'sourceOfRequest', 'idOfRequest'];

    const additionalFieldsLabels = {
        dateOfDispatch: "Дата и время отправки",
        idOfEnvelopeRequestInVIS: "Идентификатор запроса конверта в ВИС",
        idOfEnvelopeResponseInVIS: "Идентификатор ответа конверта в ВИС",
        idOfOperation: "Идентификатор операции",
        electronicAccountNumber: 'Номер электронного счета',
        dateOfAcceptanceOfOrderForExecution: "Дата приема к исполнению распоряжения",
        paymentAmount: "Сумма платежа",
        currencyCodeOfAmount: "Код валюты суммы",
        purposeOfPayment: "Назначение платежа",
        reasonForReferral: "Причина направления",
        dateOfClarificationOfPreviouslySubmittedInformation: "Дата уточнения ранее предоставленной информации",
        nameOfRecipient: "Наименование получателя",
        innOfRecipient: "ИНН получателя",
        kppOfRecipient: "КПП получателя",
        recipientsAccountNumber: "Номер счета получателя",
        nameofRecipientsBank: "Наименование банка",
        bicOfRecipientsBank: "БИК банка получателя",
        correspondentAccountOfRecipientsBank: "Корреспондентский счет банка получателя",
        nameOfPayer: "Наименование плательщика",
        innOfPayer: "ИНН плательщика",
        kppOfPayer: "КПП плательщика",
        payerAccountNumber: "Номер счета плательщика",
        nameOfPayersBank: "Наименование банка плательщика",
        bikOfPayersBank: "БИК банка плательщика",
        correspondentAccountOfPayersBank: "Корреспондентский счет банка плательщика",
        responseId: "Идентификатор ответа",
        codeOfProcessingResult: "Код результата обработки",
        dateOfResponseFormation: "Дата и время формирования ответа",
        descriptionOfProcessingResult: "Описание результата обработки",
        errorCode: "Код ошибки",
        errorDescription: "Описание ошибки",
    }

    return isCreation ? (
        <InformingAboutAcceptanceOfOrderForExecutionForm
            onSaveButtonClick={() => setIsCreation(false)}
            onCancelButtonClick={() => setIsCreation(false)}
        />
    ) : ((
        <div>
            {!isCreation && !editingCard && (
                <Row justify='space-between'>
                    <Title style={{margin: '0 0 30px 0'}} level={3}>Информирование о приеме к исполнению
                        распоряжения</Title>
                    <Button onClick={() => setIsCreation(true)} type="primary" icon={<PlusOutlined/>} size="large"/>
                </Row>
            )}

            {editingCard && (
                <InformingAboutAcceptanceOfOrderForExecutionForm
                    key={editingCard}
                    record={arrayWithInformingAboutAcceptanceOfOrderForExecution.filter((value) => value.dateOfCreation === editingCard)[0]}
                    onSaveButtonClick={() => setEditingCard(undefined)}
                    onCancelButtonClick={() => setEditingCard(undefined)}
                />
            )}

            <Col span={32}>
                {!editingCard && arrayWithInformingAboutAcceptanceOfOrderForExecution.map((card) => (
                    <Card style={{marginBottom: '20px'}} key={card.dateOfCreation} title={
                        <Row justify='space-between'>
                            <div>Статус: <Tag color="lime">{card.status}</Tag></div>
                            <div>
                                <Button
                                    style={{marginRight: '10px'}}
                                    onClick={() => setEditingCard(card.dateOfCreation)}
                                    icon={<EditOutlined/>}
                                />
                                <Button
                                    onClick={() => dispatch(deleteInformingAboutAcceptanceOfOrderForExecution(card.dateOfCreation))}
                                    icon={<DeleteOutlined/>}
                                />
                            </div>
                        </Row>
                    }>
                        <Row style={{marginBottom: '30px'}} justify={"space-between"}>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Идентификатор документа"
                                       value={card.documentId}/>
                            <Statistic style={{marginRight: '10px'}} title="Дата создания"
                                       value={formatDateToCustomString(card.dateOfCreation, 'dateOfCreation')}/>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Код статуса"
                                       value={card.statusNumber}/>
                            {/*@ts-ignore*/}
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Источник запроса" value={sourceOfRequestEnum[card.sourceOfRequest]}/>
                            <Statistic
                                style={{marginRight: '10px'}}
                                title="Идентификатор запроса"
                                value={card.idOfRequest}
                            />
                        </Row>

                        <Divider/>

                        <div style={{display: 'grid', gridTemplateColumns: 'repeat(4, 1fr)', gap: '16px'}}>
                            {isCardExpanded === card.dateOfCreation && Object.entries(card).filter(([key]) => !baseFields.includes(key))
                                .map(([key, value]) => (
                                    (value !== undefined && (
                                        (key === 'dateOfDispatch' || key === 'dateOfAcceptanceOfOrderForExecution' ||
                                            key === 'dateOfClarificationOfPreviouslySubmittedInformation' || key === 'dateOfResponseFormation') ? (
                                            <div key={key}>
                                                {/*@ts-ignore*/}
                                                <Statistic groupSeparator={''} title={additionalFieldsLabels[key]}
                                                           value={formatDateToCustomString(value, key)}
                                                />
                                            </div>
                                        ) : (
                                            <div key={key}>
                                                {/*@ts-ignore*/}
                                                <Statistic groupSeparator={''} title={additionalFieldsLabels[key]}
                                                           value={value}/>
                                            </div>
                                        )
                                    )
                                )))
                            }
                        </div>

                        <Row style={isCardExpanded === card.dateOfCreation ? {marginTop: '20px'} : undefined}>
                            <Button
                                onClick={() => setIsCardExpanded((prev) => prev === undefined ? card.dateOfCreation : undefined)}
                                icon={isCardExpanded !== card.dateOfCreation ? <DownOutlined/> : <UpOutlined/>}
                                size="middle"
                            >
                                Доп. данные
                            </Button>
                        </Row>
                    </Card>
                ))}
            </Col>
        </div>
    ))
}

export default InformingAboutAcceptanceOfOrderForExecution;
