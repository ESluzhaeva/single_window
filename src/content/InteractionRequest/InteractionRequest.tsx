import React, {useState} from 'react';
import {Col, Typography, Card, Tag, Row, Button, Statistic, Divider} from 'antd';
import {PlusOutlined, EditOutlined, DownOutlined, UpOutlined, DeleteOutlined} from '@ant-design/icons';
import {useAppSelector} from '../../store';
import InteractionRequestForm from "./InteractionRequestForm";
import {formatDateToCustomString, sourceOfRequestEnum} from "../../utils";
import {useDispatch} from "react-redux";
import {deleteInteractionRequest} from "../../store/InteractionRequest";

const {Title} = Typography;

function InteractionRequest() {
    const interactionRequests = useAppSelector(
        (state) => state.interactionRequests.interactionRequests,
    );
    const [isCreation, setIsCreation] = useState(false);
    const [editingCard, setEditingCard] = useState<string | undefined>(undefined);
    const [isCardExpanded, setIsCardExpanded] = useState<string | undefined>(undefined);
    const dispatch = useDispatch();

    const requiredFields = ['status', 'statusNumber', 'documentId', 'dateOfCreation', 'sourceOfRequest', 'idOfRequest'];
    const additionalFieldsLabels = {
        routingCodeInformationForTransferAndAccrualReceiving: "Код маршрутизации 'Предоставление информации, необходимой для перевода денежных средств или информации о начислении'",
        routingCodeRequestForInformationOnExecutionOfMoneyTransferOrder: "Код маршрутизации 'Предоставление запроса на получение информации об исполнении распоряжения о переводе денежных средств'",
        routingCodeInformationRequiredToTransferMoneyWithAcceptanceOfPayer: "Код маршрутизации 'Предоставление информации, необходимой для перевода денежных средств с акцептом плательщика'",
        reasonForReferral: "Причина направления",
        participantId: "Уникальный номер участника",
        responseId: "Идентификатор ответа",
        descriptionOfProcessingResult: "Описание результата обработки",
        errorCode: "Код ошибки",
        errorDescription: "Описание ошибки",
        linkToRemoteBankingServicePage: "Ссылка в сети интернет на страницу дистанционного банковского обслуживания",
        codeOfProcessingResult: "Код результата обработки",
        dateOfResponseFormation: "Дата и время формирования ответа",
        dateOfReceiptOfResponse: "Дата и время получения ответа",
        dateOfDispatch: "Дата и время отправки"
    }

    return isCreation ? (
        <InteractionRequestForm onSaveButtonClick={() => setIsCreation(false)}
                                onCancelButtonClick={() => setIsCreation(false)}/>
    ) : ((
        <div>
            {!isCreation && !editingCard && (
                <Row justify='space-between'>
                    <Title style={{margin: '0 0 30px 0'}} level={3}>Заявки на взаимодействие</Title>
                    <Button onClick={() => setIsCreation(true)} type="primary" icon={<PlusOutlined/>} size="large"/>
                </Row>
            )}

            {editingCard && (
                <InteractionRequestForm
                    key={editingCard}
                    record={interactionRequests.filter((value) => value.dateOfCreation === editingCard)[0]}
                    onSaveButtonClick={() => setEditingCard(undefined)}
                    onCancelButtonClick={() => setEditingCard(undefined)}
                />
            )}

            <Col span={32}>
                {!editingCard && interactionRequests.map((card) => (
                    <Card style={{marginBottom: '20px'}} key={card.dateOfCreation} title={
                        <Row justify='space-between'>
                            <div>Статус: <Tag color="lime">{card.status}</Tag></div>
                            <div>
                                <Button
                                    style={{marginRight: '10px'}}
                                    onClick={() => setEditingCard(card.dateOfCreation)}
                                    icon={<EditOutlined/>}
                                />
                                <Button
                                    onClick={() => dispatch(deleteInteractionRequest(card.dateOfCreation))}
                                    icon={<DeleteOutlined/>}
                                />
                            </div>
                        </Row>
                    }>
                        <Row style={{marginBottom: '30px'}} justify={"space-between"}>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Идентификатор документа"
                                       value={card.documentId}/>
                            <Statistic style={{marginRight: '10px'}} title="Дата создания"
                                       value={formatDateToCustomString(card.dateOfCreation, 'dateOfCreation')}/>
                            <Statistic style={{marginRight: '10px'}} groupSeparator={''} title="Код статуса"
                                       value={card.statusNumber}/>
                            {/*@ts-ignore*/}
                            <Statistic style={{marginRight: '10px'}} title="Источник запроса" value={sourceOfRequestEnum[card.sourceOfRequest]}/>
                            {card.idOfRequest && (<Statistic groupSeparator={''} title="Идентификатор запроса" value={card.idOfRequest}/>)}
                        </Row>

                        <Divider/>

                        <div style={{display: 'grid', gridTemplateColumns: 'repeat(4, 1fr)', gap: '16px'}}>
                            {isCardExpanded === card.dateOfCreation && Object.entries(card).filter(([key]) => !requiredFields.includes(key))
                                .map(([key, value]) => (
                                    (value !== undefined) && (
                                        (key === 'dateOfDispatch' || key === 'dateOfReceiptOfResponse' || key === 'dateOfResponseFormation') ? (
                                            <div key={key}>
                                                {/*@ts-ignore*/}
                                                <Statistic groupSeparator={''} title={additionalFieldsLabels[key]}
                                                           value={formatDateToCustomString(value, key)}/>
                                            </div>
                                        ) : (
                                            <div key={key}>
                                                {/*@ts-ignore*/}
                                                <Statistic groupSeparator={''} title={additionalFieldsLabels[key]}
                                                           value={value}/>
                                            </div>
                                        )
                                    )
                                ))
                            }
                        </div>

                        <Row style={isCardExpanded === card.dateOfCreation ? {marginTop: '20px'} : undefined}>
                            <Button
                                onClick={() => setIsCardExpanded((prev) => prev === undefined ? card.dateOfCreation : undefined)}
                                icon={isCardExpanded !== card.dateOfCreation ? <DownOutlined/> : <UpOutlined/>}
                                size="middle">Доп.
                                данные</Button>
                        </Row>
                    </Card>
                ))}
            </Col>
        </div>
    ))
}

export default InteractionRequest;
