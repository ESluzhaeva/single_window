import React from 'react';
import {Button, Form, Input, Select, Row, DatePicker, Col, Divider, Badge} from 'antd';
import {InteractionRequestType} from "../../types";
import dayjs from 'dayjs';
import {useAppDispatch} from "../../store";
import {addInteractionRequest, editInteractionRequest} from "../../store/InteractionRequest";

const { TextArea } = Input;

type PropsType = {
    record?: InteractionRequestType,
    onSaveButtonClick: () => void;
    onCancelButtonClick: () => void;
}

function InteractionRequestForm({record, onSaveButtonClick, onCancelButtonClick}: PropsType) {
    const [form] = Form.useForm();
    const statusFields = ['status', 'statusNumber', 'dateOfCreation'];
    const dispatch = useAppDispatch();

    return (
        <Form
            name="InteractionRequestForm"
            autoComplete="off"
            layout={'vertical'}
            form={form}
            initialValues={{
                documentId: record?.documentId,
                dateOfDispatch: record?.dateOfDispatch ? dayjs(record?.dateOfDispatch): undefined,
                dateOfReceiptOfResponse: record?.dateOfReceiptOfResponse ? dayjs(record?.dateOfReceiptOfResponse) : undefined,
                dateOfCreation: record?.dateOfCreation ? dayjs(record?.dateOfCreation) : undefined,
                sourceOfRequest: record?.sourceOfRequest,
                idOfRequest: record?.idOfRequest,
                linkToRemoteBankingServicePage: record?.linkToRemoteBankingServicePage,
                routingCodeInformationForTransferAndAccrualReceiving: record?.routingCodeInformationForTransferAndAccrualReceiving,
                routingCodeRequestForInformationOnExecutionOfMoneyTransferOrder: record?.routingCodeRequestForInformationOnExecutionOfMoneyTransferOrder,
                routingCodeInformationRequiredToTransferMoneyWithAcceptanceOfPayer: record?.routingCodeInformationRequiredToTransferMoneyWithAcceptanceOfPayer,
                reasonForReferral: record?.reasonForReferral,
                participantId: record?.participantId,
                responseId: record?.responseId,
                codeOfProcessingResult: record?.codeOfProcessingResult,
                dateOfResponseFormation: record?.dateOfResponseFormation ? dayjs(record?.dateOfResponseFormation): undefined,
                descriptionOfProcessingResult: record?.descriptionOfProcessingResult,
                errorCode: record?.errorCode,
                errorDescription: record?.errorDescription,
            }}
            onFinish={() => {
                if (form.isFieldValidating('documentId')) {
                    return;
                }

                if (record?.dateOfCreation !== undefined) {
                    if (form.getFieldValue('dateOfCreation') !== undefined) {
                        dispatch(editInteractionRequest({
                            id: record.dateOfCreation,
                            key: "dateOfCreation",
                            value: form.getFieldValue('dateOfCreation').toISOString(),
                        }));
                    }
                    Object.entries(form.getFieldsValue()).filter(([key]) => !statusFields.includes(key))
                        .map(([value]) => {
                            const isDateValue = value === 'dateOfDispatch' || value === 'dateOfReceiptOfResponse' || value === 'dateOfResponseFormation';
                            if (form.isFieldTouched(value)) {
                                dispatch(editInteractionRequest({
                                    id: form.getFieldValue('dateOfCreation').toISOString(),
                                    key: value,
                                    value: isDateValue ? form.getFieldValue(value).toISOString() : form.getFieldValue(value),
                                }));
                            }
                        });
                } else {
                    dispatch(addInteractionRequest({
                        ...form.getFieldsValue(),
                        status: 'Новый',
                        statusNumber: 1,
                        dateOfCreation: form.getFieldValue('dateOfCreation').toISOString(),
                    }))
                }
                onSaveButtonClick();
            }}
        >
            <Row>
                <Form.Item
                    name="documentId"
                    style={{width: '350px', marginRight: '50px'}}
                    label="Идентификатор документа"
                    rules={[{ required: true, message: 'Введите идентификатор документа' }]}
                >
                    <Input placeholder="Введите идентификатор документа"/>
                </Form.Item>
                <Form.Item
                    name="dateOfCreation" label="Дата и время создания"
                    rules={[{ required: true, message: 'Введите дату создания' }]}
                >
                    <DatePicker showTime placeholder="Введите дату создания" />
                </Form.Item>
            </Row>
            <Row>
                <Form.Item style={{marginRight: '50px'}} name="dateOfDispatch" label="Дата и время отправки">
                    <DatePicker showTime placeholder="Введите дату отправки" />
                </Form.Item>
                <Form.Item name="dateOfReceiptOfResponse" label="Дата и время получения ответа">
                    <DatePicker showTime placeholder="Введите дату получения ответа" />
                </Form.Item>
            </Row>
            <Col span={8}>
                <Form.Item
                    rules={[{ required: true, message: 'Выберите источник запроса' }]}
                    name="sourceOfRequest"
                    label="Источник запроса"
                >
                    <Select placeholder='Выберите источник запроса'>
                        <Select.Option value="local">Локально</Select.Option>
                        <Select.Option value="global">Глобально</Select.Option>
                    </Select>
                </Form.Item>
            </Col>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  " />
                Реквизиты запроса
            </Divider>

            <Col span={16}>
                <Form.Item name="idOfRequest" label="Идентификатор запроса">
                    <Input placeholder="Введите идентификатор запроса"/>
                </Form.Item>
            </Col>
            <Col span={16}>
                <Form.Item
                    name="linkToRemoteBankingServicePage"
                    label="Ссылка в сети интернет на страницу дистанционного банковского обслуживания"
                    rules={[{ required: true, message: 'Введите ссылку' }]}
                >
                    <TextArea
                        placeholder="Введите ссылку"
                        autoSize={{ minRows: 3, maxRows: 5 }}
                    />
                </Form.Item>
            </Col>
            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item
                        rules={[{ required: true, message: 'Введите код маршрутизации' }]}
                        name="routingCodeInformationForTransferAndAccrualReceiving"
                        label="Код маршрутизации 'Предоставление информации, необходимой для перевода денежных средств или информации о начислении'"
                    >
                        <Input placeholder="Введите код маршрутизации"/>
                    </Form.Item>
                </Col>
                <Col span={10}>
                    <Form.Item
                        rules={[{ required: true, message: 'Введите код маршрутизации' }]}
                        name="routingCodeRequestForInformationOnExecutionOfMoneyTransferOrder"
                        label="Код маршрутизации 'Предоставление запроса на получение информации об исполнении распоряжения о переводе денежных средств'"
                    >
                        <Input placeholder="Введите код маршрутизации"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item
                        rules={[{ required: true, message: 'Введите код маршрутизации' }]}
                        name="routingCodeInformationRequiredToTransferMoneyWithAcceptanceOfPayer"
                        label="Код маршрутизации 'Предоставление информации, необходимой для перевода денежных средств с акцептом плательщика'"
                    >
                        <Input placeholder="Введите код маршрутизации"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item rules={[{ required: true, message: 'Выберите причину направления'}]} name="reasonForReferral" label="Причина направления">
                        <Select style={{marginTop: '22px'}} placeholder='Выберите причину направления'>
                            <Select.Option value="creation">Новая</Select.Option>
                            <Select.Option value="editing">Обновление</Select.Option>
                            <Select.Option value="deleting">Удаление</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
            </Row>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  " />
                Реквизиты ответа
            </Divider>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="participantId" label="Уникальный номер участника">
                        <Input placeholder="Введите номер участника"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="responseId" label="Идентификатор ответа">
                        <Input placeholder="Введите идентификатор ответа"/>
                    </Form.Item>
                </Col>
            </Row>

            <Row>
                <Col style={{marginRight: '50px'}} span={8}>
                    <Form.Item name="codeOfProcessingResult" label="Код результата обработки">
                        <Input placeholder="Введите код"/>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="dateOfResponseFormation" label="Дата и время формирования ответа">
                        <DatePicker showTime placeholder="Введите дату формирования ответа" />
                    </Form.Item>
                </Col>
            </Row>

            <Col span={16}>
                <Form.Item name="descriptionOfProcessingResult" label="Описание результата обработки">
                    <TextArea
                        placeholder="Введите описание"
                        autoSize={{ minRows: 3, maxRows: 5 }}
                    />
                </Form.Item>
            </Col>

            <Divider orientationMargin="0" orientation="left">
                <Badge color="#1677ff" text="  " />
                Сведения об ошибке
            </Divider>

            <Col span={8}>
                <Form.Item name="errorCode" label="Код ошибки">
                    <Input placeholder="Введите код"/>
                </Form.Item>
            </Col>

            <Col span={16}>
                <Form.Item name="errorDescription" label="Описание ошибки">
                    <TextArea
                        placeholder="Введите описание"
                        autoSize={{ minRows: 3, maxRows: 5 }}
                    />
                </Form.Item>
            </Col>

            <Form.Item style={{marginTop: '50px'}}>
                <Button htmlType="submit" type="primary">Сохранить</Button>
                <Button onClick={onCancelButtonClick} style={{marginLeft: '20px'}} type="primary" danger>
                    Отмена
                </Button>
            </Form.Item>
        </Form>
    )
}

export default InteractionRequestForm;
