import { configureStore } from '@reduxjs/toolkit';
import interactionRequestsReducer from "./InteractionRequest";
import informationForTransferRequestsReducer from "./InformationForTransferRequest";
import arrayWithInformationForTransferWithAcceptanceReceivingReducer from "./InformationForTransferWithAcceptanceReceiving";
import arrayWithInformationForTransferAndAccrualReceivingReducer from "./InformationForTransferAndAccrualReceiving";
import arrayWithReceivingRequestForTransferInformationReducer from "./ReceivingRequestForTransferInformation";
import arrayWithInformingAboutAcceptanceOfOrderForExecutionReducer from "./InformingAboutAcceptanceOfOrderForExecution";
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

const store = configureStore({
    reducer: {
        interactionRequests: interactionRequestsReducer,
        informationForTransferRequests: informationForTransferRequestsReducer,
        arrayWithInformationForTransferWithAcceptanceReceiving: arrayWithInformationForTransferWithAcceptanceReceivingReducer,
        arrayWithInformationForTransferAndAccrualReceiving: arrayWithInformationForTransferAndAccrualReceivingReducer,
        arrayWithReceivingRequestForTransferInformation: arrayWithReceivingRequestForTransferInformationReducer,
        arrayWithInformingAboutAcceptanceOfOrderForExecution: arrayWithInformingAboutAcceptanceOfOrderForExecutionReducer,
    },
});

export default store;
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;