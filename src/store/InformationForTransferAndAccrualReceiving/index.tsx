import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
    InformationForTransferAndAccrualReceivingType
} from '../../types';

interface InformationForTransferAndAccrualReceivingSliceState {
    arrayWithInformationForTransferAndAccrualReceiving: InformationForTransferAndAccrualReceivingType[];
}

interface editInformationForTransferAndAccrualReceivingPayload {
    id: string;
    key: string;
    value: string | number;
}

const initialState: InformationForTransferAndAccrualReceivingSliceState = {
    arrayWithInformationForTransferAndAccrualReceiving: [
        {
            status: 'Новый',
            statusNumber: 1,
            documentId: 31005880696,
            dateOfCreation: new Date(2023, 10, 22, 9, 13, 33).toISOString(),
            dateOfResponseFormation: new Date(2023, 10, 22, 9, 13, 33).toISOString(),
            sourceOfRequest: "local",
            idOfRequest: "333",
            dateOfRequestFormation: new Date(2023, 11, 2, 19, 13, 33).toISOString(),
            routingCode: 1,
            reasonForReferral: 'отзыв',
            electronicAccountNumber: 12365243563,
            dateOfFormationOfElectronicAccount: new Date(2022, 6, 2).toISOString(),
            NDSRateAsPercentage: 12,
            amountOfNDS: 243211,
            invoiceAmount: 2313,
            amountToBePaid: 3425362,
            currencyCodeOfAmount: 'RUB',
            purposeOfPayment: 'string1',
            nameOfRecipient: 'Ivan',
            innOfRecipient: 1214131,
            recipientsAccountNumber: 35253622134,
            bicOfRecipientsBank: 342525,
            nameOfPayersBank: 'bank',
            innOfPayersBank: 432443,
        },
    ],
};

export const arrayWithInformationForTransferAndAccrualReceivingSlice = createSlice({
    name: 'arrayWithInformationForTransferAndAccrualReceiving',
    initialState,
    reducers: {
        addInformationForTransferAndAccrualReceiving(state, { payload }: PayloadAction<InformationForTransferAndAccrualReceivingType>) {
            Object.entries(payload).map(([key, value]) => {
                if (
                    (key === 'dateOfRequestFormation' || key === 'validityPeriod' ||
                        key === 'dateOfFormationOfElectronicAccount' || key === 'dateOfStartOfQualifiedCertificate' ||
                        key === 'dateOfEndOfQualifiedCertificate' || key === 'dateOfResponseFormation')
                    && value
                ) {
                    // @ts-ignore
                    payload[key] = value.toISOString();
                }
            })
            state.arrayWithInformationForTransferAndAccrualReceiving.push(payload);
        },
        editInformationForTransferAndAccrualReceiving(state, {payload: {id, key, value}}: PayloadAction<editInformationForTransferAndAccrualReceivingPayload>) {
            const item = state.arrayWithInformationForTransferAndAccrualReceiving.find(obj => obj.dateOfCreation === id);

            if (item) {
                // @ts-ignore
                item[key] = value;
            }
        },
        deleteInformationForTransferAndAccrualReceiving(state, {payload}: PayloadAction<string>) {
            state.arrayWithInformationForTransferAndAccrualReceiving.splice(
                state.arrayWithInformationForTransferAndAccrualReceiving
                    .findIndex(obj => obj.dateOfCreation === payload), 1);
        }
    },
});

export const {
    addInformationForTransferAndAccrualReceiving,
    editInformationForTransferAndAccrualReceiving,
    deleteInformationForTransferAndAccrualReceiving
} = arrayWithInformationForTransferAndAccrualReceivingSlice.actions;

export default arrayWithInformationForTransferAndAccrualReceivingSlice.reducer;
