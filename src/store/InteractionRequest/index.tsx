import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {InteractionRequestType} from '../../types';

interface InteractionRequestsSliceState {
    interactionRequests: InteractionRequestType[];
}

interface editInteractionRequestPayload {
    id: string;
    key: string;
    value: string | number;
}

const initialState: InteractionRequestsSliceState = {
    interactionRequests: [
        {
            status: 'Новый',
            statusNumber: 1,
            documentId: 31005880696,
            dateOfCreation: new Date(2023, 10, 22, 9, 13, 33).toISOString(),
            sourceOfRequest: 'local',
            idOfRequest: 'l_90bf0386-2ced-41cd-89f2-39b2b4cfa69b',
            linkToRemoteBankingServicePage: 'http:link/',
            routingCodeInformationForTransferAndAccrualReceiving: 1,
            routingCodeRequestForInformationOnExecutionOfMoneyTransferOrder: 2,
            routingCodeInformationRequiredToTransferMoneyWithAcceptanceOfPayer: 3,
            reasonForReferral: 'creation',
            participantId: 67,
            responseId: 23,
            errorCode: 123,
        }
    ],
};

export const interactionRequestsSlice = createSlice({
    name: 'interactionRequests',
    initialState,
    reducers: {
        addInteractionRequest(state, { payload }: PayloadAction<InteractionRequestType>) {
            Object.entries(payload).map(([key, value]) => {
                if (
                    (key === 'dateOfResponseFormation' || key === 'dateOfDispatch' || key === 'dateOfReceiptOfResponse') && value
                ) {
                    // @ts-ignore
                    payload[key] = value.toISOString();
                }
            })
            state.interactionRequests.push(payload);
        },
        editInteractionRequest(state, {payload: {id, key, value}}: PayloadAction<editInteractionRequestPayload>) {
            const item = state.interactionRequests.find(obj => obj.dateOfCreation === id);

            if (item) {
                // @ts-ignore
                item[key] = value;
            }
        },
        deleteInteractionRequest(state, {payload}: PayloadAction<string>) {
            state.interactionRequests.splice(state.interactionRequests.findIndex(obj => obj.dateOfCreation === payload), 1);
        }
    },
});

export const {
    addInteractionRequest,
    editInteractionRequest,
    deleteInteractionRequest
} = interactionRequestsSlice.actions;

export default interactionRequestsSlice.reducer;
