import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
    InformingAboutAcceptanceOfOrderForExecutionType
} from '../../types';

interface InformingAboutAcceptanceOfOrderForExecutionSliceState {
    arrayWithInformingAboutAcceptanceOfOrderForExecution: InformingAboutAcceptanceOfOrderForExecutionType[];
}

interface editInformingAboutAcceptanceOfOrderForExecutionPayload {
    id: string;
    key: string;
    value: string | number;
}

const initialState: InformingAboutAcceptanceOfOrderForExecutionSliceState = {
    arrayWithInformingAboutAcceptanceOfOrderForExecution: [
        {
            status: 'Новый',
            statusNumber: 1,
            documentId: 31005880696,
            dateOfCreation: new Date(2023, 10, 22, 9, 13, 33).toISOString(),
            sourceOfRequest: "local",
            idOfRequest: "333",
            idOfOperation: "333",
            electronicAccountNumber: 12365243563,
            dateOfAcceptanceOfOrderForExecution: new Date(2023, 11, 10).toISOString(),
            paymentAmount: 2342414,
            currencyCodeOfAmount: 'RUB',
            purposeOfPayment: 'string1',
            reasonForReferral: 'Причина напраления 1',
            nameOfRecipient: 'Ivan',
            innOfRecipient: 1214131,
            recipientsAccountNumber: 35253622134,
            bicOfRecipientsBank: 342525,
            nameOfPayer: 'bank',
            innOfPayer: 432443,
            payerAccountNumber: 1231421,
            bikOfPayersBank: 42314,
        },
    ],
};

export const arrayWithInformingAboutAcceptanceOfOrderForExecutionSlice = createSlice({
    name: 'arrayWithInformingAboutAcceptanceOfOrderForExecution',
    initialState,
    reducers: {
        addInformingAboutAcceptanceOfOrderForExecution(state, { payload }: PayloadAction<InformingAboutAcceptanceOfOrderForExecutionType>) {
            Object.entries(payload).map(([key, value]) => {
                if (
                    (key === 'dateOfDispatch' || key === 'dateOfAcceptanceOfOrderForExecution' ||
                    key === 'dateOfClarificationOfPreviouslySubmittedInformation' || key === 'dateOfResponseFormation')
                    && value
                ) {
                    // @ts-ignore
                    payload[key] = value.toISOString();
                }
            })
            state.arrayWithInformingAboutAcceptanceOfOrderForExecution.push(payload);
        },
        editInformingAboutAcceptanceOfOrderForExecution(state, {payload: {id, key, value}}: PayloadAction<editInformingAboutAcceptanceOfOrderForExecutionPayload>) {
            const item = state.arrayWithInformingAboutAcceptanceOfOrderForExecution.find(obj => obj.dateOfCreation === id);

            if (item) {
                // @ts-ignore
                item[key] = value;
            }
        },
        deleteInformingAboutAcceptanceOfOrderForExecution(state, {payload}: PayloadAction<string>) {
            state.arrayWithInformingAboutAcceptanceOfOrderForExecution.splice(
                state.arrayWithInformingAboutAcceptanceOfOrderForExecution
                    .findIndex(obj => obj.dateOfCreation === payload), 1);
        }
    },
});

export const {
    addInformingAboutAcceptanceOfOrderForExecution,
    editInformingAboutAcceptanceOfOrderForExecution,
    deleteInformingAboutAcceptanceOfOrderForExecution
} = arrayWithInformingAboutAcceptanceOfOrderForExecutionSlice.actions;

export default arrayWithInformingAboutAcceptanceOfOrderForExecutionSlice.reducer;
