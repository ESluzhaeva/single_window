import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
    ReceivingRequestForTransferInformationType
} from '../../types';

interface ReceivingRequestForTransferInformationSliceState {
    arrayWithReceivingRequestForTransferInformation: ReceivingRequestForTransferInformationType[];
}

interface editReceivingRequestForTransferInformationPayload {
    id: string;
    key: string;
    value: string | number;
}

const initialState: ReceivingRequestForTransferInformationSliceState = {
    arrayWithReceivingRequestForTransferInformation: [
        {
            status: 'Новый',
            statusNumber: 1,
            documentId: 31005880696,
            dateOfCreation: new Date(2023, 10, 22, 9, 13, 33).toISOString(),
            sourceOfRequest: "local",
            idOfRequest: "333",
            dateOfRequestFormation: new Date(2023, 11, 2, 19, 13, 33).toISOString(),
            routingCode: 123123432,
            decreeNumber: 544867038,
            dateOfDecreeCompilation: new Date(2023, 9, 12).toISOString(),
            dateOfDebiting: new Date(2022, 7, 1).toISOString(),
            paymentInRequestAmount: 12312,
            innOfPayersInRequest: 5439720589,
            payerAccountNumberInRequest: 23425223424,
        },
    ],
};

export const arrayWithReceivingRequestForTransferInformationSlice = createSlice({
    name: 'arrayWithReceivingRequestForTransferInformation',
    initialState,
    reducers: {
        addReceivingRequestForTransferInformation(state, { payload }: PayloadAction<ReceivingRequestForTransferInformationType>) {
            Object.entries(payload).map(([key, value]) => {
                if (
                    (key === 'dateOfResponseFormation' || key === 'dateOfRequestFormation' ||
                        key === 'dateOfDecreeCompilation' || key === 'dateOfDebiting' ||
                        key === 'dateOfReceipt' || key === 'dateOfDebitingOfResponse' ||
                        key === 'dateOfPlacementInArchive')
                    && value
                ) {
                    // @ts-ignore
                    payload[key] = value.toISOString();
                }
            })
            state.arrayWithReceivingRequestForTransferInformation.push(payload);
        },
        editReceivingRequestForTransferInformation(state, {payload: {id, key, value}}: PayloadAction<editReceivingRequestForTransferInformationPayload>) {
            const item = state.arrayWithReceivingRequestForTransferInformation.find(obj => obj.dateOfCreation === id);

            if (item) {
                // @ts-ignore
                item[key] = value;
            }
        },
        deleteReceivingRequestForTransferInformation(state, {payload}: PayloadAction<string>) {
            state.arrayWithReceivingRequestForTransferInformation.splice(
                state.arrayWithReceivingRequestForTransferInformation
                    .findIndex(obj => obj.dateOfCreation === payload), 1);
        }
    },
});

export const {
    addReceivingRequestForTransferInformation,
    editReceivingRequestForTransferInformation,
    deleteReceivingRequestForTransferInformation
} = arrayWithReceivingRequestForTransferInformationSlice.actions;

export default arrayWithReceivingRequestForTransferInformationSlice.reducer;
