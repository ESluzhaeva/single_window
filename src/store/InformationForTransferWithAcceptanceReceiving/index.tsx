import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {InformationForTransferWithAcceptanceReceivingType} from '../../types';

interface InformationForTransferWithAcceptanceReceivingSliceState {
    arrayWithInformationForTransferWithAcceptanceReceiving: InformationForTransferWithAcceptanceReceivingType[];
}

interface editInformationForTransferWithAcceptanceReceivingPayload {
    id: string;
    key: string;
    value: string | number;
}

const initialState: InformationForTransferWithAcceptanceReceivingSliceState = {
    arrayWithInformationForTransferWithAcceptanceReceiving: [
        {
            status: 'Новый',
            statusNumber: 1,
            documentId: 31005880696,
            dateOfCreation: new Date(2023, 10, 22, 9, 13, 33).toISOString(),
            dateOfResponseFormation: new Date(2023, 10, 22, 9, 13, 33).toISOString(),
            sourceOfRequest: "local",
            idOfRequest: "333",
            dateOfRequestFormation: new Date(2023, 11, 2, 19, 13, 33).toISOString(),
            routingCode: 1,
            acceptanceAmount: 123456,
            payerOfAcceptanceAccountNumber: 436572342,
            bikOfPayersOfAcceptanceBank: 43343434,
            nameOfFoundationDocument: 'name1',
            numberOfFoundationDocument: 123,
            dateOfFoundationDocument: new Date(2023, 10, 22).toISOString(),
            electronicAccountNumber: 12365243563,
            dateOfFormationOfElectronicAccount: new Date(2022, 6, 2).toISOString(),
            NDSRateAsPercentage: 12,
            amountOfNDS: 243211,
            invoiceAmount: 2313,
            amountToBePaid: 3425362,
            currencyCodeOfAmount: 'RUB',
            purposeOfPayment: 'string1',
            nameOfRecipient: 'Ivan',
            innOfRecipient: 1214131,
            recipientsAccountNumber: 35253622134,
            bicOfRecipientsBank: 342525,
            nameOfPayersBank: 'bank',
            innOfPayersBank: 432443,
        },
    ],
};

export const arrayWithInformationForTransferWithAcceptanceReceivingSlice = createSlice({
    name: 'arrayWithInformationForTransferWithAcceptanceReceiving',
    initialState,
    reducers: {
        addInformationForTransferWithAcceptanceReceiving(state, { payload }: PayloadAction<InformationForTransferWithAcceptanceReceivingType>) {
            Object.entries(payload).map(([key, value]) => {
                if (
                    (key === 'dateOfRequestFormation' || key === 'dateOfFoundationDocument' ||
                    key === 'dateOfExpirationOfAuthority' || key === 'validityPeriod' ||
                    key === 'dateOfFormationOfElectronicAccount' || key === 'dateOfStartOfQualifiedCertificate' ||
                    key === 'dateOfEndOfQualifiedCertificate' || key === 'dateOfResponseFormation') && value
                ) {
                    // @ts-ignore
                    payload[key] = value.toISOString();
                }
            })
            state.arrayWithInformationForTransferWithAcceptanceReceiving.push(payload);
        },
        editInformationForTransferWithAcceptanceReceiving(state, {payload: {id, key, value}}: PayloadAction<editInformationForTransferWithAcceptanceReceivingPayload>) {
            const item = state.arrayWithInformationForTransferWithAcceptanceReceiving.find(obj => obj.dateOfCreation === id);

            if (item) {
                // @ts-ignore
                item[key] = value;
            }
        },
        deleteInformationForTransferWithAcceptanceReceiving(state, {payload}: PayloadAction<string>) {
            state.arrayWithInformationForTransferWithAcceptanceReceiving.splice(
                state.arrayWithInformationForTransferWithAcceptanceReceiving
                    .findIndex(obj => obj.dateOfCreation === payload), 1);
        }
    },
});

export const {
    addInformationForTransferWithAcceptanceReceiving,
    editInformationForTransferWithAcceptanceReceiving,
    deleteInformationForTransferWithAcceptanceReceiving
} = arrayWithInformationForTransferWithAcceptanceReceivingSlice.actions;

export default arrayWithInformationForTransferWithAcceptanceReceivingSlice.reducer;
