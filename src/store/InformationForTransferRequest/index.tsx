import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {InformationForTransferRequestType} from '../../types';

interface InformationForTransferRequestsSliceState {
    informationForTransferRequests: InformationForTransferRequestType[];
}

interface editInformationForTransferRequestPayload {
    id: string;
    key: string;
    value: string | number;
}

const initialState: InformationForTransferRequestsSliceState = {
    informationForTransferRequests: [
        {
            status: 'Новый',
            statusNumber: 1,
            documentId: 31005880696,
            dateOfCreation: new Date(2023, 10, 22, 9, 13, 33).toISOString(),
            idOfUniquePayment: "43343434",
            idOfRequest: "333",
            sourceOfRequest: "local",
            NDSRateAsPercentage: 123,
            nameOfRecipient: 'name1',
            purposeOfPayment: 'string1',
            codeOfProcessingResult: 1,
            responseId: 12345,
            bicOfRecipientsBank: 435687695,
            correspondentAccountOfRecipientsBank: 23434242313,
            innOfPayersBank: 6683660,
            kppOfPayersBank: 5849068068,
            errorCode: 1,
            errorDescription: 'Error error'
        },
        {
            status: 'Новый',
            statusNumber: 1,
            documentId: 123,
            dateOfCreation: new Date(2024, 2, 1, 19, 13, 33).toISOString(),
            idOfUniquePayment: "43343434",
            idOfRequest: "333",
            sourceOfRequest: "local",
            NDSRateAsPercentage: 456,
            nameOfRecipient: 'name2',
            purposeOfPayment: 'string2',
            codeOfProcessingResult: 2,
            responseId: 4342,
            bicOfRecipientsBank: 352457665,
            correspondentAccountOfRecipientsBank: 88790435,
            innOfPayersBank: 589205,
            kppOfPayersBank: 947376983746,
            errorCode: 2,
            errorDescription: 'error description'
        }
    ],
};

export const informationForTransferRequestsSlice = createSlice({
    name: 'informationForTransferRequests',
    initialState,
    reducers: {
        addInformationForTransferRequest(state, { payload }: PayloadAction<InformationForTransferRequestType>) {
            Object.entries(payload).map(([key, value]) => {
                if (
                    (key === 'dateOfResponseFormation' || key === 'dateOfDispatch' ||
                    key === 'dateOfFormationOfElectronicAccount' || key === 'validityPeriod') && value
                ) {
                    // @ts-ignore
                    payload[key] = value.toISOString();
                }
            })
            state.informationForTransferRequests.push(payload);
        },
        editInformationForTransferRequest(state, {payload: {id, key, value}}: PayloadAction<editInformationForTransferRequestPayload>) {
            const item = state.informationForTransferRequests.find(obj => obj.dateOfCreation === id);

            if (item) {
                // @ts-ignore
                item[key] = value;
            }
        },
        deleteInformationForTransferRequest(state, {payload}: PayloadAction<string>) {
            state.informationForTransferRequests.splice(state.informationForTransferRequests.findIndex(obj => obj.dateOfCreation === payload), 1);
        }
    },
});

export const {
    addInformationForTransferRequest,
    editInformationForTransferRequest,
    deleteInformationForTransferRequest
} = informationForTransferRequestsSlice.actions;

export default informationForTransferRequestsSlice.reducer;
